wait()
vars={
	enabled = script.Parent.Enabled;
	loading = script.Parent.loading;
	surfaceGui = script.Parent.SurfaceGui;
	txtLoading = script.Parent.SurfaceGui.loadingTxt;
	powerOn = script.Parent.SurfaceGui.Power;
	windowsLogo = script.Parent.SurfaceGui.windowsLogo;
	toolbar = script.Parent.SurfaceGui.toolbar;
	powerOff = script.Parent.SurfaceGui.PowerOff;
	shutdown = script.Parent.shutdown;
	part = script.Parent;
	plr = game.Players.LocalPlayer;
	char = game.Players.LocalPlayer.Character
};
apps={
	vbsEditor = vars.surfaceGui.Apps.vbsEditor;
	musicPlayer = vars.surfaceGui.Apps.musicPlayer;
	settings = vars.surfaceGui.Apps.settings;
};
funcs={
	partToPlr=function()
		spawn(function()
			game:GetService'RunService'.Heartbeat:connect(function()
				vars.part.CFrame = CFrame.new(vars.char.Torso.CFrame.X,vars.char.Torso.CFrame.Y+3,vars.char.Torso.CFrame.Z5) * CFrame.Angles(0.1,0,0)
			end)		
		end)
	end;
	shutdownLoad=function()
		vars.shutdown.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Shutting down ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Shutting down . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Shutting down . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt  1
			end
			wait(0.5)
		end
		vars.shutdown.Value = false
		vars.powerOn.Visible = true
	end;
	loading=function()
		vars.loading.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Loading ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Loading . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Loading . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt  1
			end
			wait(0.5)
		end
		vars.loading.Value = false
		funcs.loadMainScreen()
	end;
	loadMainScreen=function()
		script.Parent.Decal.Texture = "rbxassetid://75898881"
		vars.windowsLogo.Visible = false
		vars.txtLoading.Visible = false
		vars.toolbar.Visible = true
		vars.powerOff.Visible = true
		for i,v in pairs(apps) do
			v.Visible = true
		end
	end;
	shutdown=function()
		vars.enabled.Value = false
		vars.loading.Value = false
		vars.toolbar.Visible = false
		vars.powerOff.Visible = false
		for i,v in pairs(apps) do
			v.Visible = false
		end
		script.Parent.Decal.Texture = "rbxassetid://32241532"
		funcs.shutdownLoad()
	end;
	loadWindows=function()
		vars.enabled.Value = true
		vars.powerOn.Visible = false
		vars.windowsLogo.Visible = true
		vars.txtLoading.Visible = true
		funcs.loading()
	end;
};
onClicked={
	vars.powerOn.ImageButton.MouseButton1Click:connect(function()
		funcs.loadWindows()
	end);
	vars.powerOff.ImageButton.MouseButton1Click:connect(function()
		funcs.shutdown()
	end);
};
funcs.partToPlr()