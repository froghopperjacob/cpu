Create = function(itemClass,tabl)
local item = Instance.new(itemClass)
for i,v in pairs(tabl) do
local a,b = ypcall(function() return item[i] end)
if a then
item[i] = tabl[i]
end
end
return item
end
function runDummyScript(f,scri)
local oldenv = getfenv(f)
local newenv = setmetatable({}, {
__index = function(_, k)
if k:lower() == 'script' then
return scri
else
return oldenv[k]
end
end
})
setfenv(f, newenv)
ypcall(function() f() end)
end
cors = {}
mas = Instance.new("Model",game:GetService("Lighting")) 
mas.Name = "CompiledModel"
o1 = Create("Part",{
["Parent"] = mas,
["Material"] = Enum.Material.SmoothPlastic,
["Position"] = Vector3.new(106.599998, 6.9578824, 326.950012),
["Rotation"] = Vector3.new(6.23980331, 0, 0),
["Anchored"] = true,
["CFrame"] = CFrame.new(106.599998, 6.9578824, 326.950012, 1, 0, 0, 0, 0.994075775, 0.108689979, 0, 0.108689979, 0.994075775),
["Size"] = Vector3.new(10.1999998, 9, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
})
o2 = Create("SurfaceGui",{
["Parent"] = o1,
["Face"] = Enum.NormalId.Back,
})
o3 = Create("Frame",{
["Name"] = "windowsLogo",
["Parent"] = o2,

["Position"] = UDim2.new(0.25,0,0.20000000298023,0),
["Size"] = UDim2.new(0.5,0,0.5,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o4 = Create("ImageLabel",{
["Parent"] = o3,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://15434304",
})
o5 = Create("Frame",{
["Name"] = "Power",
["Parent"] = o2,

["Position"] = UDim2.new(0.81000000238419,0,0.75,0),
["Size"] = UDim2.new(0,150,0,150),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o6 = Create("ImageButton",{
["Parent"] = o5,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://55568933",
})
o7 = Create("TextLabel",{
["Name"] = "loadingTxt",
["Parent"] = o2,

["Position"] = UDim2.new(0.050000000745058,0,0.75,0),
["Size"] = UDim2.new(0.89999997615814,0,0.20000000298023,0),
["Text"] = "Loading . . .",
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o8 = Create("Folder",{
["Name"] = "Apps",
["Parent"] = o2,
})
o9 = Create("Frame",{
["Name"] = "vbsEditor",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.0099999997764826,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o10 = Create("ImageButton",{
["Parent"] = o9,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://10354012",
})
o11 = Create("TextLabel",{
["Parent"] = o9,

["Position"] = UDim2.new(0,0,0.85000002384186,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "VBS editor",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o12 = Create("Frame",{
["Name"] = "musicPlayer",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.20999999344349,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o13 = Create("ImageButton",{
["Parent"] = o12,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://155428801",
})
o14 = Create("TextLabel",{
["Parent"] = o12,

["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "Musicplayer",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size18,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o15 = Create("Frame",{
["Name"] = "settings",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.40999999642372,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o16 = Create("ImageButton",{
["Parent"] = o15,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://227790783",
})
o17 = Create("TextLabel",{
["Parent"] = o15,

["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "Settings",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size18,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o18 = Create("Frame",{
["Name"] = "toolbar",
["Parent"] = o2,
["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0.10000000149012,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(0.129412, 0.129412, 0.129412),
["BorderSizePixel"] = 0,
})
o19 = Create("Frame",{
["Name"] = "PowerOff",
["Parent"] = o2,

["Position"] = UDim2.new(0.89999997615814,0,0.89999997615814,0),
["Size"] = UDim2.new(0.10000000149012,0,0.10000000149012,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o20 = Create("ImageButton",{
["Parent"] = o19,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://3072254",
})
o21 = Create("Folder",{
["Name"] = "templates",
["Parent"] = o2,
})
o22 = Create("Frame",{
["Name"] = "windowTemp",
["Parent"] = o21,
["Position"] = UDim2.new(0.10000000149012,0,0.050000000745058,0),
["Size"] = UDim2.new(0.80000001192093,0,0.75,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
})
o23 = Create("Frame",{
["Name"] = "topBar",
["Parent"] = o22,
["Size"] = UDim2.new(1,0,0.10000000149012,0),
["BackgroundColor3"] = Color3.new(0.313726, 0.313726, 0.313726),
})
o24 = Create("Frame",{
["Name"] = "exit",
["Parent"] = o23,
["Position"] = UDim2.new(0.91000002622604,0,0,0),
["Size"] = UDim2.new(0.090000003576279,0,1,0),
["BackgroundColor3"] = Color3.new(0.831373, 0.129412, 0.0627451),
})
o25 = Create("ImageButton",{
["Parent"] = o24,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://16201262",
})
o26 = Create("Frame",{
["Name"] = "minimize",
["Parent"] = o23,
["Position"] = UDim2.new(0.81999999284744,0,0,0),
["Size"] = UDim2.new(0.090000003576279,0,1,0),
["BackgroundColor3"] = Color3.new(0.709804, 0.709804, 0.709804),
})
o27 = Create("ImageButton",{
["Parent"] = o26,

["Position"] = UDim2.new(0,5,0,0),
["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://16381269",
})
o28 = Create("TextLabel",{
["Name"] = "topBarText",
["Parent"] = o23,

["Position"] = UDim2.new(0.10000000149012,0,0,0),
["Size"] = UDim2.new(0.60000002384186,0,1,0),
["Text"] = "asdaksdasld",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.901961, 0.901961, 0.901961),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o29 = Create("Frame",{
["Name"] = "nonBar",
["Parent"] = o22,
["Position"] = UDim2.new(0,0,0.10000000149012,0),
["Size"] = UDim2.new(1,0,0.89999997615814,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
})
o30 = Create("Folder",{
["Name"] = "standbyItems",
["Parent"] = o2,
})
o31 = Create("Folder",{
["Name"] = "windowsRunning",
["Parent"] = o2,
})
o32 = Create("Decal",{
["Parent"] = o1,
["Texture"] = "rbxassetid://32241532",
["Face"] = Enum.NormalId.Back,
})
o33 = Create("Script",{
["Parent"] = o1,
["Disabled"] = true,
})
table.insert(cors,coroutine.create(function()
wait()
runDummyScript(function()
wait()
appsRunning={};
vars={
	enabled = script.Parent.Enabled;
	loading = script.Parent.loading;
	surfaceGui = script.Parent.SurfaceGui;
	txtLoading = script.Parent.SurfaceGui.loadingTxt;
	powerOn = script.Parent.SurfaceGui.Power;
	windowsLogo = script.Parent.SurfaceGui.windowsLogo;
	toolbar = script.Parent.SurfaceGui.toolbar;
	powerOff = script.Parent.SurfaceGui.PowerOff;
	shutdown = script.Parent.shutdown;
	part = script.Parent;
	plr = game.Players.LocalPlayer;
	char = game.Players.LocalPlayer.Character
};
apps={
	vbsEditor = vars.surfaceGui.Apps.vbsEditor;
	musicPlayer = vars.surfaceGui.Apps.musicPlayer;
	settings = vars.surfaceGui.Apps.settings;
};
funcs={
	partToPlr=function()
		spawn(function()
			game:GetService'RunService'.Heartbeat:connect(function()
				vars.part.CFrame = CFrame.new(vars.char.Torso.CFrame.X,vars.char.Torso.CFrame.Y+3,vars.char.Torso.CFrame.Z5) * CFrame.Angles(0.1,0,0)
			end)		
		end)
	end;
	shutdownLoad=function()
		vars.shutdown.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Shutting down ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Shutting down . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Shutting down . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt  1
			end
			wait(0.5)
		end
		vars.shutdown.Value = false
		vars.txtLoading.Visible = false
		vars.windowsLogo.Visible = false
		vars.powerOn.Visible = true
	end;
	loading=function()
		vars.loading.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Loading ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Loading . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Loading . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt  1
			end
			wait(0.5)
		end
		vars.loading.Value = false
		funcs.loadMainScreen()
	end;
	loadMainScreen=function()
		script.Parent.Decal.Texture = "rbxassetid://75898881"
		vars.windowsLogo.Visible = false
		vars.txtLoading.Visible = false
		vars.toolbar.Visible = true
		vars.powerOff.Visible = true
		for i,v in pairs(apps) do
			v.Visible = true
		end
	end;
	shutdown=function()
		vars.enabled.Value = false
		vars.loading.Value = false
		vars.toolbar.Visible = false
		vars.powerOff.Visible = false
		vars.txtLoading.Visible = true
		vars.windowsLogo.Visible = true
		for i,v in pairs(apps) do
			v.Visible = false
		end
		script.Parent.Decal.Texture = "rbxassetid://32241532"
		funcs.shutdownLoad()
	end;
	loadWindows=function()
		vars.enabled.Value = true
		vars.powerOn.Visible = false
		vars.windowsLogo.Visible = true
		vars.txtLoading.Visible = true
		funcs.loading()
	end;
	createWindow=function(txt,code)
		local winCopy = vars.surfaceGui.templates.windowTemp:Copy()
		table.insert(appsRunning,{app=winCopy,appName=txt})
		winCopy.Parent = vars.SurfaceGui.windowsRunnings
		for i,v in pairs(core.Vars.surfaceGui.standbyItems) do
			if v.Name == txt and v.ClassName == "Folder" then
				for a,b in pairs(v:GetChildren()) do
					b.Parent = winCopy.nonBar
				end
				break
			end
		end
		winCopy.Name = txt
		winCopy.topBar.topBarText = txt
		winCopy.Visible = true
		winCopy.topBar.exit.MouseButton1Click:connect(function()
			if winCopy then
				for i,v in pairs(appsRunning) do
					if v.appName == winCopy.Name then
						table.remove(appsRunning,i)
					end
				end 
				winCopy:remove()
			end
		end)
		winCopy.topBar.minimize.MouseButton1Click:connect(function()
			if winCopy then
				winCopy.Visible = false
			end
		end)
		local codeRun = coroutine.new(code)
		coroutine.resume(codeRun)
	end
};
onClicked={
	vars.powerOn.ImageButton.MouseButton1Click:connect(function()
		funcs.loadWindows()
	end);
	vars.powerOff.ImageButton.MouseButton1Click:connect(function()
		funcs.shutdown()
	end);
};
funcs.partToPlr()
end,o33)
end))
o34 = Create("BoolValue",{
["Name"] = "Enabled",
["Parent"] = o1,
})
o35 = Create("BoolValue",{
["Name"] = "loading",
["Parent"] = o1,
})
o36 = Create("BoolValue",{
["Name"] = "shutdown",
["Parent"] = o1,
})
mas.Parent = workspace
mas:MakeJoints()
local mas1 = mas:GetChildren()
for i=1,#mas1 do
	mas1[i].Parent = workspace 
	ypcall(function() mas1[i]:MakeJoints() end)
end
mas:Destroy()
for i=1,#cors do
coroutine.resume(cors[i])
end
