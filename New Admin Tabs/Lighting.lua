local player = game.Players.LocalPlayer
local mouse = player:GetMouse()
mouse.Button1Down:connect(function()
	ray=function(hit)
		local ray = Ray.new(player.Character.Head.CFrame.p, (hit - player.Character.Head.CFrame.p).unit * 300)
		local part, position = workspace:FindPartOnRay(ray, player.Character, false, true)

		local beam = Instance.new("Part", workspace)
		beam.BrickColor = BrickColor.new("Bright red")
		beam.FormFactor = "Custom"
		beam.Material = "Neon"
		beam.Transparency = 1 --0.25
		beam.Anchored = true
		beam.Locked = true
		beam.CanCollide = false

		local distance = (player.Character.Head.CFrame.p - position).magnitude
		beam.Size = Vector3.new(0.3, 0.3, distance)
		beam.CFrame = CFrame.new(player.Character.Head.CFrame.p, position) * CFrame.new(0, 0, -distance / 2)

		game:GetService("Debris"):AddItem(beam, 0.1)
		if part then
			if part.Parent:FindFirstChild("Humanoid") ~= nil then
				part.Parent:FindFirstChild("Humanoid"):TakeDamage(10)
			end
		end
	end
	for i=1,360 do
		ray
	end
	ray()
end)