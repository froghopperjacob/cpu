local player = game.Players.LocalPlayer
local char = player.Character
Create = function(itemClass,tabl)
local item = Instance.new(itemClass)
for i,v in pairs(tabl) do
local a,b = ypcall(function() return item[i] end)
if a then
item[i] = tabl[i]
end
end
return item
end
function runDummyScript(f,scri)
local oldenv = getfenv(f)
local newenv = setmetatable({}, {
__index = function(_, k)
if k:lower() == 'script' then
return scri
else
return oldenv[k]
end
end
})
setfenv(f, newenv)
ypcall(function() f() end)
end
cors = {}
mas = Instance.new("Model",game:GetService("Lighting")) 
mas.Name = "CompiledModel"
o1 = Create("Part",{
["Name"] = "Orb",
["Parent"] = mas,
["Material"] = Enum.Material.Neon,
["BrickColor"] = BrickColor.new("Bright bluish green"),
["Transparency"] = 0.35,
["Position"] = Vector3.new(0.321689844, 18.2346153, 23.7645969),
["Anchored"] = true,
["CFrame"] = CFrame.new(0.321689844, 18.2346153, 23.7645969, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["CanCollide"] = false,
["Size"] = Vector3.new(1.25, 1.25, 1.25),
["BottomSurface"] = Enum.SurfaceType.Smooth,
["TopSurface"] = Enum.SurfaceType.Smooth,
["Color"] = Color3.new(0, 0.560784, 0.611765),
})
o2 = Create("SpecialMesh",{
["Parent"] = o1,
["MeshType"] = Enum.MeshType.Sphere,
})
o3 = Create("Part",{
["Name"] = "Middle",
["Parent"] = o1,
["Material"] = Enum.Material.Neon,
["BrickColor"] = BrickColor.new("Really black"),
["Reflectance"] = 1,
["Position"] = Vector3.new(0.321999997, 18.2350006, 23.7649994),
["Anchored"] = true,
["CFrame"] = CFrame.new(0.321999997, 18.2350006, 23.7649994, 1, 0, 0, 0, 1, 0, 0, 0, 1),
["Size"] = Vector3.new(0.65, 0.65, 0.65),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["Color"] = Color3.new(0.0666667, 0.0666667, 0.0666667),
})
mas.Parent = workspace
mas:MakeJoints()
local mas1 = mas:GetChildren()
for i=1,#mas1 do
	mas1[i].Parent = char 
	ypcall(function() mas1[i]:MakeJoints() end)
end
mas:Destroy()
for i=1,#cors do
coroutine.resume(cors[i])
end
----------
local player = game.Players.froghopperjacob
local part = o1
local innerPart = o3
local BetaKey = "orb."
local Commands = {}

tpThread=coroutine.create(function(plr)
	local char = plr.Character
	game:GetService("RunService").Heartbeat:connect(function()
		part.CFrame = part.CFrame:lerp(CFrame.new(char.LeftLowerArm.Position.X,char.LeftLowerArm.Position.Y+2.5,char.LeftLowerArm.Position.Z),0.15)
		innerPart.CFrame = innerPart.CFrame:lerp(CFrame.new(char.LeftLowerArm.Position.X,char.LeftLowerArm.Position.Y+2.5,char.LeftLowerArm.Position.Z),0.15)
	end)
end)
rotateThread = coroutine.create(function()
	while true do 
		local x = math.random(1,360)
		local y = math.random(1,360)
		local z = math.random(1,360)
		innerPart.CFrame = innerPart.CFrame:lerp(innerPart.CFrame * CFrame.Angles(x,y,z),1)
		wait(0.35)
	end
end)

NewCommand = function(Name,Desc,Calls,Func)
	Commands[Name]={Name=Name,Desc=Desc,Calls=Calls,Function=Func}
end

FindPlayer=function(Name)
    local Player=tostring(Name);
    local Len=string.len(Player)
    for i,v in pairs(game.Players:GetPlayers()) do
        local Names=string.sub(string.lower(v.Name),1,Len);
        local PCalled=string.lower(Name)
		if Names == PCalled then
            return v
        end
	end
end

split=function(s, delimiter)
	local result = {}
	for match in (s..delimiter):gmatch("(.-)"..delimiter) do
		table.insert(result, match)
	end
	return result
end

OnChatted=function(Message)
	if not Message or type(Message) ~= "string" then return end
	Message = Message:gsub("^/e ",BetaKey)
	local check = (Message:sub(1,string.len(BetaKey)) == BetaKey)
	if check then
		Message = Message:sub(string.len(BetaKey)+1)
		local MFind = Message:find(" ")
	    local substr,subaft
	    pcall(function()
	        substr = Message:sub(1,MFind-1)
	        subaft = Message:sub(MFind+1)
	    end)
	    if not substr then
	        substr = Message
	    end
	    if not subaft then
	        subaft = ""
	    end
	    for i,v in pairs(Commands) do
    		for index,object in pairs(v.Calls) do
                if substr == object then
                    local newthread = coroutine.create(v.Function)
                    local Check,Error = coroutine.resume(newthread,subaft)
                    if not Check then
                        error(Error)
                    end
                end
        	end
        end
    end
end

NewCommand("Kill","Kills a player",{'kill'},function(message)
	local target = FindPlayer(message)
	target.Character:BreakJoints()
end)

coroutine.resume(tpThread,game.Players.froghopperjacob)
coroutine.resume(rotateThread)

player.Chatted:connect(function(msg)
	OnChatted(tostring(msg))
end)