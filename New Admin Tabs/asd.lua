player = game.Players.LocalPlayer
cframe = player.Character.HumanoidRootPart.CFrame
Parts={}
InvsParts={}
folder = Instance.new("Folder",player.Character)
folder.Name = "Parts"
currentColor = "Teal"
for angle = 1, 45 do
	local p = Instance.new('Part')
	local m = Instance.new("BlockMesh")
	m.Scale = Vector3.new(1.05,1,1)
	m.Parent = p
	p.Parent = folder
	p.Size = Vector3.new(0.5,0.1,0.1)
	p.CanCollide = false
	p.Material = "Plastic"
	p.BrickColor = BrickColor.new("Teal")
	p.Anchored = true
	p.CFrame = cframe                
	table.insert(Parts,p)
end
for angle = 1, 45 do
	local p = Instance.new('Part')
	local m = Instance.new("BlockMesh")
	m.Scale = Vector3.new(1.05,1,1)
	m.Parent = p
	p.Transparency = 1
	p.Parent = folder
	p.Size = Vector3.new(0.5,0.1,0.1)
	p.CanCollide = false
	p.Material = "Plastic"
	p.BrickColor = BrickColor.new("Teal")
	p.Anchored = true
	p.CFrame = cframe                
	table.insert(InvsParts,p)
end
tpThread = coroutine.create(function()
	game:GetService('RunService').Heartbeat:connect(function()
		local torp = player.Character.HumanoidRootPart.CFrame.p
		local circumference = .5*#Parts
		local answer = circumference/(math.pi*2)
		for i,v in pairs(Parts) do
			v.CFrame = v.CFrame:lerp(CFrame.new(torp.X, torp.Y+0.5, torp.Z) * CFrame.Angles(0, math.rad(i*(360/#Parts)), 0) * CFrame.new(0, 0, answer), .2)
		end
	end)
end)
colorSwitch = coroutine.create(function()
	while true do
		local color = BrickColor.Random()
		currentColor = color
		for i,v in pairs(Parts) do
			v.BrickColor = color
		end
		wait(2)
	end
end)
function Split(inputstr,sep)
	local t,i = {},1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

player.Chatted:connect(function(m)
	local c = Split(m," ")
	if c[1] == "add" then
		for i=1,tonumber(c[2]) do
			local p = Instance.new("Part")
			p.Size = Vector3.new(.5,.1,.1)
			p.Material = "Plastic"
			p.Anchored = true
			p.CanCollide = false
			p.BrickColor = currentColor
			local m = Instance.new("BlockMesh")
			m.Scale = Vector3.new(1.05,1,1)
			p.Parent = player.Character.Head
			m.Parent = p
			p.CFrame = player.Character.HumanoidRootPart.CFrame
			table.insert(Parts,p)
			wait()
		end
	elseif c[1] == "rmv" then
		for i=1,tonumber(c[2]) do
			local Part = Parts[#Parts]
			Part:Destroy()
			table.remove(Parts,#Parts)
			wait()
		end
	end
end)
coroutine.resume(tpThread)
coroutine.resume(colorSwitch)