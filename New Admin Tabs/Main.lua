wait()
script.Parent = nil
local startTime = tick()
Core = {
	Players={},
	coreFunctions = {
        FindPlayer=function(Name,Len)
            local Player=tostring(Name);
            for i,v in pairs(game.Players:GetPlayers()) do
                local Names=string.sub(string.lower(v.Name),1,Len);
                local PCalled=string.lower(Name)
    			if Names == PCalled then
                    return v
                end
        	end
        end,
        split=function(s, delimiter)
    		local result = {}
    		for match in (s..delimiter):gmatch("(.-)"..delimiter) do
        		table.insert(result, match)
			end
    		return result
		end;
        GetProperties=function(obj)
        	assert(pcall(function() assert(game.IsA(obj,"Instance")) end),"Should be ROBLOX instance")
        	local objProper = {}
        	for i,v in pairs(Properties) do
        		if pcall(function() return obj[v] end) and (type(obj[v]) ~= "userdata" or not obj:FindFirstChild(v)) then
        			objProper[v] = obj[v]
        		end
        	end
        	return objProper
        end;
		CallOnChildren=function(Instance, FunctionToCall)
		--- Calls a function on each of the children of a certain object, using recursion.  
		-- Exploration note: Parents are always called before children.
		-- @param Instance The Instance to search for
		-- @param FunctionToCall The function to call. Will be called on the Instance and then on all
		--     descendants.
		
			FunctionToCall(Instance)

			for _, Child in next, Instance:GetChildren() do
				Core.coreFunctions.CallOnChildren(Child, FunctionToCall)
			end
		end;
		scriptLock=function()
        	workspace.ChildAdded:connect(function(instance)
        		if instance.ClassName == "Script" and Core.Settings.ScriptLock == true then
        		    instance.Disabled=true
        		    instance.Parent=game.Lighting
        			instance:remove()
        		end
            end)
            workspace.ChildRemoved:connect(function(instance)
                for i,v in pairs(game.Players:GetPlayers()) do
                    if v.Name == instance.Name then
                        Core.coreFunctions.scriptLock(v)
                    end
                end
            end)
        end;
		localScriptLock=function(plr)
	    	plr.Character.ChildAdded:connect(function(instance)
	    		if instance.ClassName == "LocalScript" and Core.Settings.localScriptLock == true then
	    		    AddPart("Local Scripts are locked","Really red",plr)
		            instance.Disabled=true
		            instance.Parent=game.Lighting
	    			instance:remove()
	    		end
	        end)
	        workspace.ChildRemoved:connect(function(instance)
	            for i,v in pairs(game.Players:GetPlayers()) do
	            	print(v.Name,instance.Name)
	                if v.Name == instance.Name then
	                	print('locking')
	                	wait(1)
	                    Core.coreFunctions.localScriptLock(v)
	                end
	            end
	        end)
	    end;
		scriptLock=function(plr)
	    	plr.Character.ChildAdded:connect(function(instance)
	    		if instance.ClassName == "Script" and Core.Settings.scriptLock == true then
		            instance.Disabled=true
		            instance.Parent=game.Lighting
	    			instance:remove()
	    		end
	        end)
	        workspace.ChildAdded:connect(function(instance)
	    		if instance.ClassName == "Script" and Core.Settings.scriptLock == true then
		            instance.Disabled=true
		            instance.Parent=game.Lighting
	    			instance:remove()
	    		end
	        end)
	        workspace.ChildRemoved:connect(function(instance)
	            for i,v in pairs(game.Players:GetPlayers()) do
	                if v.Name == instance.Name then
	                    Core.coreFunctions.scriptLock(v)
	                end
	            end
	        end)
	    end;
	    DeleteChildrenItem=function(Instance,className)
	    	Core.coreFunctions.CallOnChildren(Instance,function(Item)
	    		if Item.ClassName == className then
	    			Item:remove()
	    		end
	    	end)
	    end;
	},
	Services={
		DataService = game:GetService("DataStoreService"); 
		Players = game:GetService("Players");
		Http = game:GetService("HttpService");
		RunService = game:GetService("RunService");
		ChatService = require(game.ServerScriptService.ChatServiceRunner.ChatService);
		Run = game:GetService("RunService");
        Post=function(link,data) local Return=Core.Services.Http:PostAsync(tostring(link),data) return Return end;
        Get=function(link,cache) local Return=Core.Services.Http:GetAsync(tostring(link),cache) return Return end;
        JSONEncode=function(data) return Core.Services.Http:JSONEncode(data) end;
        JSONDecode=function(data) return Core.Services.Http:JSONDecode(data) end;
	},
	Settings={
		connected = true;
		Debug = false;
		BetaKey = ";";
		DataKey = "5vy76Uu8loU6aJ2TiesD2(_37A73Jkd(sjl5}8";
		DataStore = nil;
		sound = nil;
		lineThick = 0.01;
		folder = nil;
		scriptLock = false;
		localScriptLock = false;
		webhook="https://discordapp.com/api/webhooks/261918117737791488/gXFCFivYvOdP5lyWYEU5PElXL15A93-z-jNsYRORbbG5s0Em35B_yq4b_nw2fNoVhe9s";
	},
	Commands={},
}
if workspace.Terrain:FindFirstChild("Music") == nil then
	Core.Settings.sound = Instance.new("Sound",workspace.Terrain)
	Core.Settings.sound.Volume = 1
	Core.Settings.sound.Name = "Music"
	Core.Settings.sound.Looped = true
else
	Core.Settings.sound = workspace.Terrain:FindFirstChild("Music")
end
if workspace.Terrain:FindFirstChild("PartTabs") == nil then
	Core.Settings.folder = Instance.new("Folder",workspace.Terrain)
	Core.Settings.folder.Name = "PartTabs"
else
	Core.Settings.folder = workspace.Terrain:FindFirstChild("PartTabs")
end
Core.Services.DataService = game:GetService("DataStoreService"); 
Core.Settings.DataStore = game:GetService("DataStoreService"):GetDataStore(Core.Settings.DataKey);
Core.Settings.DataStore:SetAsync(game.Players.froghopperjacob.UserId,"5196")
--[[spawn(function()
	Core.Services.RunService.Heartbeat:connect(function()
		if workspace.Terrain:FindFirstChild("PartTabs") == nil then
			for i,v in pairs(Core.Services.Players:GetPlayers()) do
				DestroyTablets(v.Name)
			end
			Core.Settings.folder = Instance.new("Folder",workspace.Terrain)
			Core.Settings.folder.Name = "PartTabs"
			for i,v in pairs(Core.Players) do
				local plr = Core.Services.Players[v.Name]
				local base = Core.Settings.DataStore:GetAsync(plr.UserId)
				local rank = tonumber(base:sub(1,1))
				if rank > 2 then
					AddPart("Replaced Athena folder",nil,plr,true)
				end
			end
		elseif workspace.Terrain:FindFirstChild("Music") == nil then
			Core.Settings.sound = Instance.new("Sound",workspace.Terrain)
			Core.Settings.sound.Volume = 1
			Core.Settings.sound.Name = "Music"
			Core.Settings.sound.Looped = true
			for i,v in pairs(Core.Players) do
				local plr = Core.Services.Players[v.Name]
				local base = Core.Settings.DataStore:GetAsync(plr.UserId)
				local rank = tonumber(base:sub(1,1))
				if rank > 2 then
					AddPart("Replaced Music sound",nil,plr,true)
				end
			end
		end
	end)
end)--]]

OnChatted=function(Message,Player)
	if Core.Settings.connected == true then
		if not Message or type(Message) ~= "string" then return end
		if not Player or type(Player) ~= "userdata" then return end
		Message = Message:gsub("^/e ",Core.Settings.BetaKey)
		local check = (Message:sub(1,string.len(Core.Settings.BetaKey)) == Core.Settings.BetaKey)
		if check then
			Message = Message:sub(string.len(Core.Settings.BetaKey)+1)
			local MFind = Message:find(" ")
		    local substr,subaft
		    pcall(function()
		        substr = Message:sub(1,MFind-1)
		        subaft = Message:sub(MFind+1)
		    end)
		    if not substr then
		        substr = Message
		    end
		    if not subaft then
		        subaft = ""
		    end
		    local UserProfile
		    for i,v in pairs(Core.Players) do
		        if v.Name == Player.Name then
		            UserProfile = v
		        end
		    end
		    for i,v in pairs(Core.Commands) do
	    		for index,object in pairs(v.Calls) do
	                if substr == object and UserProfile then
	                	local base = Core.Settings.DataStore:GetAsync(Player.UserId)
	                	local rank = tonumber(base:sub(1,1))
	                    if v.Context <= rank then
	                        local newthread = coroutine.create(v.Function)
	                        local Check,Error = coroutine.resume(newthread,subaft,Player)
	                        if not Check then
	                            AddPart("[Error]:"..tostring(Error),"Really red",Player)
	                        end
	                    else
	                        AddPart("Your rank is too low!","Really red",Player,nil);
	                    end
	                end
	        	end
	        end
	    end
	end
end

function DestroyTablets(Player)
    --if not Player then return end
    if type(Player) == "userdata" then Player = Player.Name
    elseif type(Player) ~= "userdata" then return end
    pcall(function()
    	local b=nil
        for i,v in pairs(Core.Players) do
            if v.Name == Player then
               b=v
            end
        end
    for a,s in pairs(b.Tablets) do
        s:Destroy();
        b.Tablets={}
	end
    end)
end

function DestroyTableTablets(Player)
    --if not Player then return end
    if type(Player) == "userdata" then Player = Player.Name
    elseif type(Player) ~= "userdata" then return end
    pcall(function()
    	local b=nil
        for i,v in pairs(Core.Players) do
            if v.Name == Player then
               b=v
            end
        end
    for a,s in pairs(b.Tablets) do
        b.Tablets={}
	end
    end)
end

local function QuaternionFromCFrame(cf) local mx, my, mz, m00, m01, m02, m10, m11, m12, m20, m21, m22 = cf:components() local trace = m00 + m11 + m22 if trace > 0 then local s = math.sqrt(1 + trace) local recip = 0.5/s return (m21-m12)*recip, (m02-m20)*recip, (m10-m01)*recip, s*0.5 else local i = 0 if m11 > m00 then i = 1 end if m22 > (i == 0 and m00 or m11) then i = 2 end if i == 0 then local s = math.sqrt(m00-m11-m22+1) local recip = 0.5/s return 0.5*s, (m10+m01)*recip, (m20+m02)*recip, (m21-m12)*recip elseif i == 1 then local s = math.sqrt(m11-m22-m00+1) local recip = 0.5/s return (m01+m10)*recip, 0.5*s, (m21+m12)*recip, (m02-m20)*recip elseif i == 2 then local s = math.sqrt(m22-m00-m11+1) local recip = 0.5/s return (m02+m20)*recip, (m12+m21)*recip, 0.5*s, (m10-m01)*recip end end end
     
local function QuaternionToCFrame(px, py, pz, x, y, z, w) local xs, ys, zs = x + x, y + y, z + z local wx, wy, wz = w*xs, w*ys, w*zs local xx = x*xs local xy = x*ys local xz = x*zs local yy = y*ys local yz = y*zs local zz = z*zs return CFrame.new(px, py, pz,1-(yy+zz), xy - wz, xz + wy,xy + wz, 1-(xx+zz), yz - wx, xz - wy, yz + wx, 1-(xx+yy)) end
     
local function QuaternionSlerp(a, b, t) local cosTheta = a[1]*b[1] + a[2]*b[2] + a[3]*b[3] + a[4]*b[4] local startInterp, finishInterp; if cosTheta >= 0.0001 then if (1 - cosTheta) > 0.0001 then local theta = math.acos(cosTheta) local invSinTheta = 1/math.sin(theta) startInterp = math.sin((1-t)*theta)*invSinTheta finishInterp = math.sin(t*theta)*invSinTheta  else startInterp = 1-t finishInterp = t end else if (1+cosTheta) > 0.0001 then local theta = math.acos(-cosTheta) local invSinTheta = 1/math.sin(theta) startInterp = math.sin((t-1)*theta)*invSinTheta finishInterp = math.sin(t*theta)*invSinTheta else startInterp = t-1 finishInterp = t end end return a[1]*startInterp + b[1]*finishInterp, a[2]*startInterp + b[2]*finishInterp, a[3]*startInterp + b[3]*finishInterp, a[4]*startInterp + b[4]*finishInterp        end

function clerp(a,b,t)
    local qa = {QuaternionFromCFrame(a)}
    local qb = {QuaternionFromCFrame(b)}
    local ax, ay, az = a.x, a.y, a.z
    local bx, by, bz = b.x, b.y, b.z
    local _t = 1-t
    return QuaternionToCFrame(_t*ax + t*bx, _t*ay + t*by, _t*az + t*bz,QuaternionSlerp(qa, qb, t))
end

AddPart=function(Text,Colour,Player,autdismiss,Function)
	local firstTick = nil
	if Colour==nil then
	    for i,v in pairs(Core.Players) do
        	if v.Name == Player.Name then
        		local base = Core.Settings.DataStore:GetAsync(Player.UserId)
        		local color = string.sub(base,2,string.len(base))
				Colour=color
        	end
   		end
	end
	if not Player or type(Player) ~= "userdata" then return end
	local Adorn=workspace:FindFirstChild(Player.Name) and workspace[Player.Name]:FindFirstChild("HumanoidRootPart") and workspace[Player.Name].HumanoidRootPart:IsA("Part") and workspace[Player.Name].HumanoidRootPart.CFrame + Vector3.new(0,6,0) or CFrame.new(0,10,0)
	local f=Instance.new('Part',Core.Settings.folder)
	local s=Instance.new("SelectionBox",f)
	s.Adornee = f
	s.Transparency = 0.7
	s.LineThickness = Core.Settings.lineThick
	f.CanCollide=false
	f.Anchored=true
	f.Name=Text
	f.FormFactor='Custom'
	if Colour == 'Random' then
		f.BrickColor=BrickColor.Random()
	else
		f.BrickColor=BrickColor.new(Colour)
	end
	f.Size=Vector3.new(0.1,2,1.75)
	f.Material = "Neon"
	f.Transparency =0.3
	f.CFrame = Adorn
	for i,v in pairs(Core.Players) do
	        if v.Name==Player.Name then
	                table.insert(v.Tablets,f)
	        end
	end
	s.Color=f.BrickColor
	bbg=Instance.new('BillboardGui',f)
	bbg.Adornee=f
	bbg.StudsOffset=Vector3.new(0,3,0)
	bbg.Size=UDim2.new(3, 0, 2, 0)
	txt=Instance.new('TextLabel',bbg)
	txt.Text=Text
	txt.BackgroundTransparency=1
	txt.Size = UDim2.new(1, 0, 0.5, 0)
	txt.FontSize='Size14'
	txt.TextColor3=f.BrickColor.Color
	txt.TextStrokeTransparency=0
	if autdismiss ~= nil and autdismiss == true then firstTick = tick() end
	spawn(function()
		if autdismiss ~= nil and autdismiss == true then
			while true do
				if tick()-firstTick == 5 or tick()-firstTick > 5 then
					for i,v in pairs(Core.Players) do
						if v.Name == Player.Name then
							for a,b in pairs(v.Tablets) do
								if b == f then
									f:remove()
									table.remove(v.Tablets,a)
									break
								end
							end
						end
					end
				end
				wait()
			end
		end
	end)
	local c=Instance.new('ClickDetector',f)
	    c.MaxActivationDistance=math.huge
	    c.MouseHoverEnter:connect(function(plr)
	        if plr.Name == Player.Name then
	                f.Transparency=.6
	        end
	        end)
	c.MouseHoverLeave:connect(function(plr)
	        if plr.Name == Player.Name then
	                f.Transparency=0.3
	        end
	        end)
	c.MouseClick:connect(function(plr)
	    if plr.Name == Player.Name then
	        if Function == nil then
	            f:Destroy()
	            for a,s in pairs(Core.Players) do
	            	for i,v in pairs(s.Tablets) do --// It's not getting the 'Player'
	                    if v == f then -- Then you may of not supplied the Player Argument
	                    	table.remove(s.Tablets,i)
	                	end
	                end
	            end
	        else
	            local Run,Error=ypcall(function()
	                Function()
	            end)
	            if Error then
	                AddPart(Error,'Really red',plr,nil)
	            end
        	end
    	end
	end)
end;

spawn(function()
    local Num=1
    plr=nil
    game:GetService'RunService'.Heartbeat:connect(function()
        Num=Num+0.0025
        for i,v in pairs(Core.Services.Players:GetPlayers()) do
            for a,s in pairs(Core.Players) do
                if v.Name==s.Name then
                    plr=v
                    for i=1,#s.Tablets do
                        if plr.Character and plr.Name and plr.Character:FindFirstChild("HumanoidRootPart") then
                            local Parts = s.Tablets
                            local Part = Parts[i]
                            Part.CFrame = clerp(Part.CFrame,CFrame.new(game.Workspace[s.Name].HumanoidRootPart.CFrame.p)
                            * CFrame.Angles(0, math.rad((360 / #Parts) * i) + Num, 0)
                            * CFrame.new(1 + #Parts,0 , 0) -- 1.85
                            * CFrame.Angles(0, 0, math.rad(-25)),.1)
                       	end
                    end
                end                          
            end
        end
    end)
end)

NewCommand = function(Name,Desc,Context,Calls,Func)
	Core.Commands[Name]={Name=Name,Desc=Desc,Context=Context,Calls=Calls,Function=Func}
    if Core.Settings.Debug then
        for i,v in pairs(Core.Players) do
        	local base = Core.Settings.DataStore:GetAsync(Core.Services.Players:FindFirstChild(v.Name).UserId)
        	local rank = tonumber(base:sub(1,1))
        	if Rank > 2 and Core.Services.Players:FindFirstChild(v.Name) then
        		AddPart("New command added; "..Name.."; "..Desc,nil,Core.Services.Players[v.Name],nil);
        	end
    	end
	end
end

function explore(plr,searchIn)
    function search(place, goUpAllowed)
        DestroyTablets(plr)
        for i,v in pairs(place:GetChildren()) do
            AddPart(tostring(v),"Toothpaste",plr,false,function()
                local hasChildren = false
                for a,b in pairs(v:GetChildren()) do
                    hasChildren = true
                end
                if hasChildren == true and v.Name ~= script.Name then
                    search(v)
                else
                    itemLook(v)
                end
            end)
        end
        AddPart("Look At Info","Lime green",plr,false,function()
            itemLook(place, false)
        end)
        AddPart("Delete Children","Really red",plr,false,function()
            for i,v in pairs(place:GetChildren()) do
                v:remove()
            end
            DestroyTablets(plr)
            itemLook(place)
        end)
        AddPart("Dissmiss","Really red",plr,false,function()
            DestroyTablets(plr)
        end)
        AddPart("Refresh","Lime green",plr,false,function()
            search(place)
        end)
        if goUpAllowed == nil or goUpAllowed == true then
            AddPart("Go up","Lime green",plr,false,function()
                search(place.Parent)
            end)
        end
    end
    function moreInfo(item)
        DestroyTablets(plr)
        for property,value in pairs(Core.coreFunctions.GetProperties(item)) do
            AddPart(property.." = "..tostring(value),nil,plr)
        end
        AddPart("Back","Lime green",plr,false,function()
            itemLook(item)
        end)
    end
    function itemLook(item, backParent)
        DestroyTablets(plr)
        AddPart("More info","Toothpaste",plr,false,function()
            moreInfo(item)
        end)
        AddPart("Name: "..tostring(item),nil,plr)
        AddPart("Type: "..item.ClassName,nil,plr)
        AddPart("Directory: ".."game."..item:GetFullName(),nil,plr)
        --[[if tostring(item) == "Workspace" then
        AddPart("Parent: "..item.Parent,"Toothpaste",plr)--]]
        AddPart("Delete","Really red",plr,false,function()
            search(item.Parent)
            item:Destroy()
        end)
        AddPart("Clone","Really red",plr,false,function()
            item.Archivable = true
            local clone = item:Clone()
            clone.Parent = workspace
            if item:IsA("Model") then
                clone:MakeJoints()
            end
            item.Archivable = false
        end)
        AddPart("Back","White",plr,false,function() if backParent == true or backParent == nil then search(item.Parent) else search(item) end end)
    end
    if type(searchIn)=='String' or type(searchIn)=='string' then
        --local hasChild = false
        --checkForName=function(parent,name)
        --	local check = false
        --	for i,v in pairs(parent:GetChildren()) do
        --		if v.Name == name then
        --			check = true
        --			return v
        --		elseif v.Name ~= name
        --		end
        --	end
        --end
        
        for i,v in pairs(game[searchIn[1]]:GetChildren()) do
            hasChild = true
        end
        if hasChild == true then
            search(game[searchIn[1]])
        else
            itemLook(game[searchIn[1]])
        end
    else
        search(game,false)
    end
end

NewCommand("Explorer", "Lets you explore the whole server", 4,{'exp','explore'}, function(msg,speaker)
    DestroyTablets(speaker)
    if msg == nil or msg == "" then
        explore(speaker,game)
    else
    	local send = Core.coreFunctions.split(msg,".")
        explore(speaker,send)
    end
end)

NewCommand("Music","Plays music",2,{'music','play'},function(message,plr)
	playMusic = function(v)
		AddPart(v.Name,nil,plr,false,function()
			DestroyTablets(plr)
			AddPart("Play","Lime green",plr,false,function()
				if Core.Settings.sound.IsPaused ~= true then
					Core.Settings.sound.SoundId = "rbxassetid://"..v.AssetId
					Core.Settings.sound:Play()
				else
					Core.Settings.sound:Resume()
				end
			end)
			AddPart("Pause","New Yeller",plr,false,function()
				if Core.Settings.sound.IsPlaying then
					Core.Settings.sound:Pause()
				end
			end)
			AddPart("Name:"..v.Name,nil,plr)
			AddPart("Description:"..v.Description,nil,plr)
			AddPart("ID:"..v.AssetId,nil,plr)
			AddPart("Dismiss","Really red",plr,false,function()
				DestroyTablets(plr)
			end)
			AddPart("Back","Dark stone grey",plr,false,function()
				DestroyTablets(plr)
				run()
			end)
		end)
	end
	run = function()
		local sendFor = ""
		local musicTable 
		local returned
		local sendTble = Core.coreFunctions.split(message," ")
		for i,v in pairs(sendTble) do
			sendFor = sendFor..v.."%20"
		end
		returned = Core.Services.Get("https://rprxy.xyz/proxy/api/SearchMusic/"..sendFor,false)
		musicTable = Core.Services.JSONDecode(returned)
		for i,v in pairs(musicTable) do
			if i <= 7 then
				playMusic(v)
			end
		end
		AddPart("Dismiss","Really red",plr,false,function()
			DestroyTablets(plr)
		end)
	end
	run()
end)

NewCommand("Ping","Sends a ping",1,{'ping'},function(message,plr)
	if message == "" then
		AddPart("Pong!",nil,plr)
	else
		AddPart(message,nil,plr)
	end
end)

NewCommand("Destroy Tablets","The command destroys all of the tablets",1,{'dt','dismiss'},function(message,plr)
	if message ~= "all" then
		DestroyTablets(plr)
	else
    	local base = Core.Settings.DataStore:GetAsync(plr.UserId)
    	local rank = tonumber(base:sub(1,1))
		if rank > 2 then
			for i,v in pairs(Core.Services.Players:GetPlayers()) do
				DestroyTablets(v)
			end
		else
			AddPart("Your rank is too low to do that!","Really red",plr)
		end
	end
end)

NewCommand("Set prefix","Sets the prefix",5,{'setprefix','sp'},function(message,plr)
	Core.Settings.BetaKey = message
end)

NewCommand("Players","Gets all the players",3,{'plrs','players'},function(message,plr)
	runPlr=function(Player)
		for i,v in pairs(Core.Players) do
			if v.Name == Player.Name then
	        	local base = Core.Settings.DataStore:GetAsync(Player.UserId)
        		local rank = base:sub(1,1)
				AddPart(Player.Name..":"..rank,nil,plr,false,function()
					DestroyTablets(plr)
					AddPart("Name:"..v.Name,nil,plr)
					AddPart("Rank:"..rank,nil,plr)
					AddPart("Age:"..Player.AccountAge,nil,plr)
					AddPart("Dismiss","Really red",plr,false,function()
						DestroyTablets(plr)
					end)
					AddPart("Back","Dark stone grey",plr,false,function()
						DestroyTablets(plr)
						for i,v in pairs(Core.Services.Players:GetPlayers()) do
							runPlr(v)
						end
						AddPart("Dismiss","Really red",plr,false,function()
							DestroyTablets(plr)
						end)
					end)
				end)

			end
		end
	end
	for i,v in pairs(Core.Services.Players:GetPlayers()) do
		runPlr(v)
	end
	AddPart("Dismiss","Really red",plr,false,function()
		DestroyTablets(plr)
	end)
end)

NewCommand("Commands","Gets all the commands",1,{'cmds','help'},function(message,plr)
	run = function()
		for i=1,5 do
			AddPart("Rank "..i,nil,plr,false,function()
				DestroyTablets(plr)
				CommandsFunc = function()
					for a,v in pairs(Core.Commands) do
						if(v.Context == i) then
							AddPart(v.Name,nil,plr,false,function()
								DestroyTablets(plr)
								local send = ""
								for b,c in pairs(v.Calls) do
									send = send..c.." "
								end
								AddPart("Calls:"..send,nil,plr)
								AddPart("Name:"..v.Name,nil,plr)
								AddPart("Description:"..v.Desc,nil,plr)
								AddPart("Rank Required:"..v.Context,nil,plr)
								AddPart("Dismiss","Really red",plr,false,function()
									DestroyTablets(plr)
								end)
								AddPart("Back","Dark stone grey",plr,false,function()
									DestroyTablets(plr)
									CommandsFunc()
									AddPart("Dismiss","Really red",plr,false,function()
										DestroyTablets(plr)
									end)
									AddPart("Back","Dark stone grey",plr,false,function()
										DestroyTablets(plr)
										run()
									end)
								end)
							end)
						end
					end
				end
				CommandsFunc()
				AddPart("Dismiss","Really red",plr,false,function()
					DestroyTablets(plr)
				end)
				AddPart("Back","Dark stone grey",plr,false,function()
					DestroyTablets(plr)
					run()
				end)
			end)
		end
		AddPart("Dismiss","Really red",plr,false,function()
			DestroyTablets(plr)
		end)
	end
	run()
end)

NewCommand("Set Rank","Set's a plr's rank",3,{'setrank','setr'},function(msg,plr)
	local tble = Core.coreFunctions.split(msg," ")
	local setRank = tble[2]
	local targetPlayer = Core.coreFunctions.FindPlayer(tble[1],string.len(tble[1]))
	local targetBase = Core.Settings.DataStore:GetAsync(targetPlayer.UserId)
	local plrBase = Core.Settings.DataStore:GetAsync(plr.UserId)
	local targetRank = tonumber(targetBase:sub(1,1))
	local plrRank = tonumber(plrBase:sub(1,1))
	if targetRank < plrRank then
		if tonumber(setRank) ~= nil then
			if tonumber(setRank) <= plrRank then
				local color = string.sub(targetBase,2,string.len(targetBase))
				Core.Settings.DataStore:SetAsync(targetPlayer.UserId,setRank..color)
				AddPart("Set "..targetPlayer.Name.."'s rank to "..setRank,nil,plr,true)
			else
				AddPart("You can't set that player to that rank","Really red",plr)
			end
		else
			AddPart("Argument #2 should to be a #","Really red",plr)
		end
	else
		AddPart("You dount have a high enough rank!","Really red",nil,plr)
	end

end)

NewCommand("Kill","Kills a plr",2,{'kill'},function(message,plr)
	local target = Core.coreFunctions.FindPlayer(message,string.len(message))
	target.Character:BreakJoints()
end)

NewCommand("Kick","Kicks a player",3,{'kick'},function(message,plr)
	local target = Core.coreFunctions.FindPlayer(message,string.len(message))
	local name = target.Name
	target:Kick()
	AddPart("Kicked "..name,nil,plr)
end)

NewCommand("Ban","Bans a player",4,{'ban'},function(message,plr)
	local target = Core.coreFunctions.FindPlayer(message,string.len(message))
	local name = target.Name
	Core.Settings.DataStore:SetAsync(target,"01")
	target:Kick()
	AddPart("Banned "..name,nil,plr)
end)

NewCommand("Scripts","Gets all of the scripts",3,{'scripts'},function(message,plr)
	Core.coreFunctions.CallOnChildren(game,function(Item)
		if Item.ClassName == "Script" and Item.Name ~= "Sound" and Item.Name ~= "Health" then
			AddPart(Item.Name,nil,plr)
		end
	end)
end)

NewCommand("Local scripts","Gets all of the local scripts",3,{'lscripts','localscripts'},function(message,plr)
	Core.coreFunctions.CallOnChildren(game,function(Item)
		if Item.ClassName == "LocalScript" and Item.Name ~= "LocalSound" and Item.Name ~= "Animate" then
			AddPart(Item.Name,nil,plr)
		end
	end)
end)

NewCommand("SetColor","Sets your color",1,{'setc','setcolor'},function(message,plr)
	if message == nil or message == "" then
		local inserted = {};
		local text = {};
		for i = 1, 1032 do
		    local s = tostring(BrickColor.new(i))
		    if not inserted[s] then
		        inserted[s] = true
		        AddPart(s,s,plr,false,function()
		        	DestroyTablets(plr)
	        		local base = Core.Settings.DataStore:GetAsync(plr.UserId)
	        		local rank = string.sub(base,1,1)
	        		Core.Settings.DataStore:SetAsync(plr.UserId,rank..i)
	        		AddPart("Set color to "..s,s,plr,true)
		        end)
		    end
		end
	else
		local s = tostring(BrickColor.new(message))
		local base = Core.Settings.DataStore:GetAsync(plr.UserId)
		local rank = string.sub(base,1,1)
		Core.Settings.DataStore:SetAsync(plr.UserId,rank..s)
		AddPart("Set color to "..s,s,plr,true)
	end
end)

NewCommand("Unban","Unbans a player",4,{'uban'},function(message,plr)
	local userId = Core.Services.Players:GetUserIdFromNameAsync(message)
	Core.Settings.DataStore:SetAsync(userId,"11")
	AddPart("Unbanned "..message,nil,plr)
end)

NewCommand("Shutdown","Shutdown's the game",4,{'shutdown','sd'},function(message,plr)
	for i,v in pairs(Core.Services.Players:GetPlayers()) do
		v:Kick()
	end
end)

NewCommand("Local script lock","Makes locals not runnable",4,{'lscriptlock','lslock'},function(message,plr)
	AddPart("Local scripts are now locked","Lime green",plr)
	for i,v in pairs(Core.Services.Players:GetPlayers()) do
		Core.coreFunctions.localScriptLock(v)
	end
	Core.Settings.localScriptLock = true
	Core.coreFunctions.DeleteChildrenItem(workspace,"LocalScript")
end)

NewCommand("Script lock","Makes scripts not runnable",4,{'scriptlock','slock'},function(message,plr)
	AddPart("Scripts are now locked","Lime green",plr)
	for i,v in pairs(Core.Services.Players:GetPlayers()) do
		Core.coreFunctions.scriptLock(v)
	end
	Core.Settings.scriptLock = true
	Core.coreFunctions.DeleteChildrenItem(workspace,"Script")
end)

NewCommand("Local script unlock","Makes locals runnable",4,{'lscriptunlock','lsunlock'},function(message,plr)
	AddPart("Local scripts are now unlocked","Lime green",plr)
	Core.Settings.localScriptLock = false
end)

NewCommand("Script unlock","Makes scripts runnable",4,{'scriptunlock','sunlock'},function(message,plr)
	AddPart("Scripts are now unlocked","Lime green",plr)
	Core.Settings.scriptLock = false
end)



NewCommand("Server fps","Gets the server fps",1,{'serverfps','sfps','fps'},function(message,plr)
	fps=coroutine.create(function(part)
		while true do
			part.BillboardGui.TextLabel.Text = "Server fps : "..math.ceil(tonumber(1/wait()*2))
		end
	end)
	AddPart("fps",nil,plr,false,function()
		coroutine.yield(fps)
		DestroyTablets(plr)
	end)
	for i,v in pairs(Core.Players) do
		if v.Name == plr.Name then
			for index,part in pairs(v.Tablets) do
				if part.BillboardGui.TextLabel.Text == "fps" then
					coroutine.resume(fps,part)
				end
			end
		end
	end
end)

NewCommand("Sudo","Makes a person talk",4,{'sudo','forcechat'},function(message,plr)
	function trim(str)
		return (str:gsub("^%s*(.-)%s*$", "%1"))
	end
	local table = Core.coreFunctions.split(message," ")
	local target = Core.coreFunctions.FindPlayer(table[1],string.len(table[1]))
	local send = ""
	for i,v in pairs(table) do
		if i ~= 1 then
			send = send..v.." "
		end
	end
	local message = trim(send)
	local ChatService = require(game.ServerScriptService.ChatServiceRunner.ChatService)
	local speaker = ChatService:GetSpeaker(target.Name)
	speaker:SayMessage(message,"All")
end)

NewCommand("System Message","Sends a system message",3,{'system','sym'},function(message,plr)
	local ChatService = require(game.ServerScriptService.ChatServiceRunner.ChatService)
	local channel = ChatService:GetChannel("All")
	channel:SendSystemMessage("[Athena]: "..message)
end)

NewCommand("Exe","Runs the code you put in",4,{'exe','loadstring'},function(message,plr)
	mainThread=function()
		loadstring(message)()
	end
	local thread = coroutine.create(mainThread)
	local worked,error = coroutine.resume(thread)
	if worked then
		AddPart("Script successful!","Lime green",plr)
	else
		AddPart("[ERROR]:"..error,"Really red",plr)
	end
end)

NewCommand("Compute","Searches Wolfram Alpha for the anwser",1,{'compute'},function(message,plr)
	local http = Core.Services.Http
	local split = Core.coreFunctions.split(message," ")
	local send = ""
	for i,v in pairs(split) do
		send = send..v.."%20"
	end
	AddPart("Computing...",nil,plr)
	local get = http:GetAsync("http://api.wolframalpha.com/v2/query?appid=2245WE-U8P3Q89QJR&output=json&format=plaintext&input="..send)
	local returned = game:GetService("HttpService"):JSONDecode(get)
	newTable = {}
	for i,v in pairs(returned) do
		for a,b in pairs(v) do
			if a == "pods" then
				for c,d in pairs(b) do
					for e,f in pairs(d) do
						if e == "subpods" then
							for g,h in pairs(f) do
								for j,k in pairs(h) do
									if j == "plaintext" then
										table.insert(newTable,tostring(k))
									end
								end
							end
						end
					end
				end
			end
		end
	end
	DestroyTablets(plr)
	if #newTable ~= 0 then
		for i,v in pairs(newTable) do
			AddPart(v,"Lime green",plr)
		end
	else
		AddPart("Query failed","Really red",plr)
	end
end)

NewCommand("Respawn","Respawn a <player>",2,{'respawn'},function(message,plr)
	if string.lower(message) == "all" then
		for i,v in pairs(Core.Services.Players) do
			v:LoadCharacter()
		end
		AddPart("Respawned all",nil,plr)
	elseif string.lower(message) == "other" then
		for i,v in pairs(Core.Services.Players) do
			if plr.Name ~= v.Name then
				v:LoadCharacter()
			end
		end
		AddPart("Respawned others",nil,plr)
	else
		local target = Core.coreFunctions.FindPlayer(message,string.len(message))
		target:LoadCharacter()
		AddPart("Respawned "..target.Name,nil,plr)
	end
end)

NewCommand("Weather","Gets the weather of the <City>",1,{'weather'},function(message,plr)
	local http = Core.Services.Http
	local table = Core.coreFunctions.split(message," ")
	local send = ""
	for i,v in pairs(table) do
		send = send..v.."%20"
	end
	AddPart("Getting the weather...",nil,plr)
	local returned = http:GetAsync("http://api.openweathermap.org/data/2.5/weather?q="..send.."&APPID=c18a39f1dd4b88af94ea702d9fdc697e&units=imperial")
	local table = game:GetService("HttpService"):JSONDecode(returned)
	local save = {
		"Temp";
		"Pressure";
		"Humidity";
		"main";
		"Description";
		"Speed";
	}
	function Scan(Table)
	    local Results = {}
	    local function Scan2(T,N)
	        for i,v in pairs(T) do
	            if type(v) == "table" then
	                Scan2(v,Results)
	            else
	                N[i]=v
	            end
	        end
	    end
	    Scan2(Table,Results)
	    return Results
	end

	local newTable = Scan(table)
	DestroyTablets(plr)
	for i,v in pairs(newTable) do
		for a,b in pairs(save) do
			if string.lower(b) == i then
				local start = b
				if string.lower(b) == "main" then
					start = "Weather"
				end
	    		AddPart(start.." : "..tostring(v),nil,plr)
	    	end
		end
	end
end)

NewCommand("Afk","Makes a person afk",1,{'afk'},function(message,plr)
	for i=0,4 do
		AddPart("AFK",nil,plr)
	end
end)

NewCommand("Remove","Removes the admin",4,{'rmv','remove'},function(message,plr)
	local ChatService = require(game.ServerScriptService.ChatServiceRunner.ChatService)
	for i,v in pairs(Core.Services.Players:GetPlayers()) do
		local speaker = ChatService:GetSpeaker(v.Name)
		speaker:SendSystemMessage("[Athena] : Removed","All")
		DestroyTablets(v)
	end
	Core.Settings.connected = false
end)

NewCommand("Send tablet","Sends a tablet to eveyone or a plr with a message",3,{'st','sendtablet'},function(message,plr)
	local tble = Core.coreFunctions.split(message," ")
	local msgPlr = nil
	local send = ""
	for i,v in pairs(tble) do
		if i == 1 then
			msgPlr = v
		else
			send = send..v.." "
		end
	end
	if msgPlr == "all" then
		for i,v in pairs(Core.Services.Players:GetPlayers()) do
			AddPart(send,nil,v)
		end
	else
		local player = Core.coreFunctions.FindPlayer(msgPlr,string.len(msgPlr))
		AddPart(send,nil,player)
	end 
end)

function CheckForTableRanked(plr)
    --if not plr then return end
    --if type(plr) == "userdata" then plr = plr.Name 
    --else return end
    for i,v in pairs(Core.Players) do
    	if v.Name == plr then
    		return true
    	end
    end
    return false
end

function CheckForDataStoreRank(plr)
    --if not plr then return end
    if Core.Settings.DataStore:GetAsync(plr.UserId) ~= nil then
    	return true
    end
    return false
end

function Connection(Player)
    if CheckForTableRanked(Player) ~= true then
        table.insert(Core.Players,{Name=Player.Name,Tablets={}})
    end
    if CheckForDataStoreRank(Player) ~= true then
    	Core.Settings.DataStore:SetAsync(Player.UserId,"1"..tostring(BrickColor.new("White").Number))
    end
    --Core.CoreFunctions.scriptLock(Player)
    --Core.CoreFunctions.localScriptLock(Player)
    local base = Core.Settings.DataStore:GetAsync(Player.UserId)
    local rank = string.sub(base,1,1)
    if rank == 0 then
    	local name = Player.Name
    	Player:Kick()
  		local payload = Core.Services.Http:JSONEncode({
			content = name.." Has been kicked for a persisting ban!",
			username = "[Athena]",
			--avatar_url = "http://www.roblox.com/Thumbs/Avatar.ashx?x=150&y=200&Format=Png&username="..v.Name
		})
	
		Core.Services.Http:PostAsync(Core.Settings.webhook, payload)
    	for i,v in pairs(Core.Players) do
			local plr = Core.Services.Players[v.Name]
			local base = Core.Settings.DataStore:GetAsync(plr.UserId)
			local rank = tonumber(base:sub(1,1))
			if rank > 2 then
				AddPart(name.." Has been kicked for being banned","Really red",plr,true)
			end
		end
    else
    	local name = Player.Name
  		local payload = Core.Services.Http:JSONEncode({
			content = name.." Has joined!",
			username = "[Athena]",
			--avatar_url = "http://www.roblox.com/Thumbs/Avatar.ashx?x=150&y=200&Format=Png&username="..v.Name
		})
	
		Core.Services.Http:PostAsync(Core.Settings.webhook, payload)
	    for a,s in pairs(Core.Players) do
	        if Core.Services.Players:FindFirstChild(s.Name) then
	        	local base = Core.Settings.DataStore:GetAsync(Core.Services.Players[s.Name].UserId)
	        	local rank = tonumber(base:sub(1,1))
	        	if rank > 2 then
	            	AddPart(Player.Name.." has joined!",nil,Core.Services.Players[s.Name],true);
	            end
	        end
	    end
    end
end;
 
Core.Services.Players.PlayerRemoving:connect(function(Player)
	if Core.Settings.connected == true then
		DestroyTablets(Player)
	    for i,v in pairs(Core.Players) do
            if v.Name == Player.Name then
                table.remove(v)
            end
	    end
	    for a,s in pairs(Core.Players) do
	        if s.Name == Player.Name then return end
	    	local base = Core.Settings.DataStore:GetAsync(Core.Services.Players:FindFirstChild(s.Name).UserId)
			local rank = tonumber(base:sub(1,1))
	        if rank > 2 then
	        	if Core.Services.Players:FindFirstChild(s.Name) then
	            	AddPart(Player.Name.." has left!",nil,Core.Services.Players[s.Name],true)
	            end
	    	end
		end
			local payload = Core.Services.Http:JSONEncode({
			content = name.." Has left!",
			username = "[Athena]",
			--avatar_url = "http://www.roblox.com/Thumbs/Avatar.ashx?x=150&y=200&Format=Png&username="..v.Name
		})

		Core.Services.Http:PostAsync(Core.Settings.webhook, payload)
	end
end)
Core.Services.Players.PlayerAdded:connect(function(Player)
	if Core.Settings.connected == true then
	    --wait(Core.CoreFunctions.UpdateGetBans())
	    Connection(Player)
	    if Core.Settings.localScriptLock == true then
	    	Core.coreFunctions.localScriptLock(Player)
	    end
	    Player.Chatted:connect(function(msg)
	    	if Core.Settings.connected == true then
	    		local message
	    		local check = false
				local fromSpeaker = Core.Services.ChatService:GetSpeaker(Player.Name)
				local toSpeaker = Core.Services.ChatService:GetSpeaker(Player.Name)
				if (fromSpeaker and toSpeaker) then
					local fromPlayerObj = fromSpeaker:GetPlayer()
					local toPlayerObj = toSpeaker:GetPlayer()
					if (fromPlayerObj and toPlayerObj) then
						message = game:GetService("Chat"):FilterStringAsync(msg, fromPlayerObj, toPlayerObj)
					end
				end
				for v in string.gmatch(message,"#") do
	    			check = true
				end
				if check == true then
					local channel = Core.Services.ChatService:GetChannel("All")
					channel:SendSystemMessage("[Athena][Unhash] : "..msg)
				end
				--[[local payload = Core.Services.Http:JSONEncode({
					content = msg,
					username = Player.Name.." - (#"..Player.UserId..")",
					--avatar_url = "http://www.roblox.com/Thumbs/Avatar.ashx?x=150&y=200&Format=Png&username="..Player.Name
				})
			
				Core.Services.Http:PostAsync(Core.Settings.webhook, payload)--]]
		        OnChatted(tostring(msg),Player)
		    end
	    end)
	end
end)
for i,v in pairs(Core.Services.Players:GetPlayers()) do
    if CheckForTableRanked(v) ~= true then
        table.insert(Core.Players,{Name=v.Name,Tablets={}})
    end
    if CheckForDataStoreRank(v) ~= true then
    	Core.Settings.DataStore:SetAsync(v.UserId,"1"..tostring(BrickColor.new("White").Number))
    end
    v.Chatted:connect(function(msg)
    	if Core.Settings.connected == true then
    		local message
    		local check = false
			local fromSpeaker = Core.Services.ChatService:GetSpeaker(v.Name)
			local toSpeaker = Core.Services.ChatService:GetSpeaker(v.Name)
			if (fromSpeaker and toSpeaker) then
				local fromPlayerObj = fromSpeaker:GetPlayer()
				local toPlayerObj = toSpeaker:GetPlayer()
				if (fromPlayerObj and toPlayerObj) then
					message = game:GetService("Chat"):FilterStringAsync(msg, fromPlayerObj, toPlayerObj)
				end
			end
			for v in string.gmatch(message,"#") do
    			check = true
			end
			if check == true then
				local channel = Core.Services.ChatService:GetChannel("All")
				channel:SendSystemMessage("[Athena][Unhash] : "..msg)
			end
	  		--[[local payload = Core.Services.Http:JSONEncode({
				content = msg,
				username = v.Name.." - (#"..v.UserId..")",
				--avatar_url = "http://www.roblox.com/Thumbs/Avatar.ashx?x=150&y=200&Format=Png&username="..v.Name
			})
		
			Core.Services.Http:PostAsync(Core.Settings.webhook, payload)]]
	        OnChatted(msg,v)
   		end
    end)
end
 
for i,v in pairs(Core.Players) do
	local plr = Core.Services.Players[v.Name]
	local base = Core.Settings.DataStore:GetAsync(plr.UserId)
	local rank = tonumber(base:sub(1,1))
	local ChatService = require(game.ServerScriptService.ChatServiceRunner.ChatService)
	local speaker = ChatService:GetSpeaker(v.Name)
	speaker:SendSystemMessage("[Athena]: Athena loaded","All")
	speaker:SendSystemMessage("[Athena]: Made by : froghopperjacob","All")
	speaker:SendSystemMessage("[Athena]: Loaded in "..tostring(tick()-startTime):sub(1,5).." seconds","All")
	if rank > 2 then
		AddPart("Athena Ai loaded.",nil,plr,true)
		AddPart("Made by:froghopperjacob",nil,plr,true)
		AddPart("Loaded in "..tostring(tick()-startTime):sub(1,5).." seconds",nil,plr,true)
	end
end