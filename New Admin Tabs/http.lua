local post = Instance.new("RemoteFunction",game.ReplicatedStorage)
post.Name = "HttpPost"
local get = Instance.new("RemoteFunction",game.ReplicatedStorage)
get.Name = "HttpGet"
local http = game:GetService("HttpService")
function post.OnServerInvoke(player,url,data)
	local returned = http:PostAsync(url,data)
	return returned
end
function get.OnServerInvoke(player,url)
	local returned = http:GetAsync(url)
	return returned
end