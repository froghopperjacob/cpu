wait(1)
player = game.Players.LocalPlayer
cframe = player.Character.HumanoidRootPart.CFrame
Parts={}
currentColor = BrickColor.new("Maroon")
currentMat = "Granite"
folder = Instance.new("Folder",player.Character)
folder.Name = "Parts"
for angle = 1, 45 do
	local p = Instance.new('Part')
	local m = Instance.new("BlockMesh")
	m.Scale = Vector3.new(1.05,1,1) -- 10.5,1,1
	m.Parent = p
	p.Parent = folder
	p.Size = Vector3.new(0.5,0.1,0.1)
	p.CanCollide = false
	p.Material = currentMat
	p.BrickColor = currentColor
	p.Anchored = true
	p.CFrame = cframe                
	table.insert(Parts,p)
end
tpThread = coroutine.create(function()
	game:GetService('RunService').Heartbeat:connect(function()
		local torp = player.Character.HumanoidRootPart.Position
		local circumference = .5*#Parts
		local answer = circumference/(math.pi*2)
		for i,v in pairs(Parts) do
			v.CFrame = v.CFrame:lerp(CFrame.new(torp.X, torp.Y-3, torp.Z) * CFrame.Angles(0, math.rad(i*(360/#Parts)), 0) * CFrame.new(0, 0, answer), .2) --2.2 for r15|-3 for r6/r5
			v.Size = v.Size:lerp(Vector3.new(.5,.1,.1),.02)
		end
	end)
end)
function trim(str)
	return (str:gsub("^%s*(.-)%s*$", "%1"))
end
function Split(inputstr,sep)
	local t,i = {},1
	for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
		t[i] = str
		i = i + 1
	end
	return t
end

player.Chatted:connect(function(m)
	local c = Split(m," ")
	if c[1] == "add" then
		for i=1,tonumber(c[2]) do
			local p = Instance.new("Part")
			p.Size = Vector3.new(.5,.1,.1)
			p.Material = currentMat
			p.Anchored = true
			p.CanCollide = false
			p.BrickColor = currentColor
			local m = Instance.new("BlockMesh")
			m.Scale = Vector3.new(1.05,1,1)
			p.Parent = player.Character.Head
			m.Parent = p
			p.CFrame = player.Character.HumanoidRootPart.CFrame
			table.insert(Parts,p)
			wait()
		end
	elseif c[1] == "rmv" then
		for i=1,tonumber(c[2]) do
			local Part = Parts[#Parts]
			Part:Destroy()
			table.remove(Parts,#Parts)
			wait()
		end
	elseif c[1] == "mat" then
		currentMat = c[2]
		for i,v in pairs(Parts) do
			v.Material = c[2]
		end
	elseif c[1] == "color" then
		local send = ""
		for i,v in pairs(c) do
			if i ~= 1 then
				send = send..v.." "
			end
		end
		send = trim(send)
		currentColor = BrickColor.new(send)
		for i,v in pairs(Parts) do
			v.BrickColor = BrickColor.new(send)
		end
	end
end)
coroutine.resume(tpThread)
sound = workspace.Terrain:WaitForChild("Music")
spawn(function()
	local SelectedPart = 0
	while true do
		if SelectedPart < #Parts then
			SelectedPart = SelectedPart + 1
		else
			SelectedPart = 1
		end
		local sel = Parts[SelectedPart]
		sel.Size = sel.Size:lerp(Vector3.new(.5,sound.PlaybackLoudness/80,.1),1)
		wait()
	end
end)