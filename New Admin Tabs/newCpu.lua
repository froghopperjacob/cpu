--wait()
Core = {
	requires = {
		loading = require(601889716);
		sideText = require(601890468);
		Start = require(601890948);
		main = require(601890101);
	},
	vars = {
		player = game.Players.froghopperjacob;
		char = game.Players.froghopperjacob.Character;
		part;
		version = "1.0.0";
	},
	functions = {
		TweenPositionWithTransparency;
		TweenTrasparency;
		createApp;
		createWindow;
	},
}
runTp=function(part)
	spawn(function()
		game:GetService("RunService").Heartbeat:connect(function()
			local torp = Core.vars.char.UpperTorso.CFrame
			part.CFrame = part.CFrame:lerp(torp * CFrame.new(0,5,-5),.2)
		end)
	end)
end
Core.functions.TweenPositionWithTransparency = Core.requires.main.TweenPositionWithTransparency
Core.functions.TweenTrasparency = Core.requires.main.TweenTrasparency
wait(Core.requires.loading.loadAthena(Core.vars.char))
Core.vars.part = Core.vars.char:WaitForChild("Athena")
wait(runTp(Core.vars.part))
wait(1)
wait(Core.requires.loading.loadSideText(Core.vars.part.Gui.Main.SideText))
wait(Core.requires.Start.runStart(Core.vars.part,Core.vars.version))
Core.requires.sideText.loginText(Core.vars.part)
Core.requires.sideText.appText(Core.vars.part)
Core.requires.sideText.logoffText(Core.vars.part)