Create = function(itemClass,tabl)
local item = Instance.new(itemClass)
for i,v in pairs(tabl) do
local a,b = ypcall(function() return item[i] end)
if a then
item[i] = tabl[i]
end
end
return item
end
function runDummyScript(f,scri)
local oldenv = getfenv(f)
local newenv = setmetatable({}, {
__index = function(_, k)
if k:lower() == 'script' then
return scri
else
return oldenv[k]
end
end
})
setfenv(f, newenv)
ypcall(function() f() end)
end
cors = {}
mas = Instance.new("Model",game:GetService("Lighting")) 
mas.Name = "CompiledModel"
o1 = Create("Part",{
["Parent"] = mas,
["Transparency"] = 0.8,
["Name"] = "Computer",
["CanCollide"] = false,
["Material"] = Enum.Material.SmoothPlastic,
["Position"] = Vector3.new(-106.599998, 6.9578824, -326.950012),
["Rotation"] = Vector3.new(-6.23980331, 0, -0),
["Anchored"] = true,
["CFrame"] = CFrame.new(-106.599998, 6.9578824, -326.950012, 1, 0, 0, 0, 0.994075775, 0.108689979, 0, -0.108689979, 0.994075775),
["Size"] = Vector3.new(10.1999998, 9, 0.200000003),
["BackSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["BottomSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["FrontSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["LeftSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["RightSurface"] = Enum.SurfaceType.SmoothNoOutlines,
["TopSurface"] = Enum.SurfaceType.SmoothNoOutlines,
})
o2 = Create("SurfaceGui",{
["Parent"] = o1,
["Face"] = Enum.NormalId.Back,
})
o3 = Create("Frame",{
["Name"] = "windowsLogo",
["Parent"] = o2,

["Position"] = UDim2.new(0.25,0,0.20000000298023,0),
["Size"] = UDim2.new(0.5,0,0.5,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o4 = Create("ImageLabel",{
["Parent"] = o3,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://15434304",
})
o5 = Create("Frame",{
["Name"] = "Power",
["Parent"] = o2,

["Position"] = UDim2.new(0.81000000238419,0,0.75,0),
["Size"] = UDim2.new(0,150,0,150),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o6 = Create("ImageButton",{
["Parent"] = o5,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://55568933",
})
o7 = Create("TextLabel",{
["Name"] = "loadingTxt",
["Parent"] = o2,

["Position"] = UDim2.new(0.050000000745058,0,0.75,0),
["Size"] = UDim2.new(0.89999997615814,0,0.20000000298023,0),
["Text"] = "Loading . . .",
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o8 = Create("Folder",{
["Name"] = "Apps",
["Parent"] = o2,
})
o9 = Create("Frame",{
["Name"] = "vbsEditor",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.0099999997764826,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o10 = Create("ImageButton",{
["Parent"] = o9,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://10354012",
})
o11 = Create("TextLabel",{
["Parent"] = o9,

["Position"] = UDim2.new(0,0,0.85000002384186,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "VBS editor",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o12 = Create("Frame",{
["Name"] = "musicPlayer",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.20999999344349,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o13 = Create("ImageButton",{
["Parent"] = o12,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://155428801",
})
o14 = Create("TextLabel",{
["Parent"] = o12,
["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "Musicplayer",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size18,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o15 = Create("Frame",{
["Name"] = "settings",
["Parent"] = o8,

["Position"] = UDim2.new(0.0099999997764826,0,0.40999999642372,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o16 = Create("ImageButton",{
["Parent"] = o15,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://227790783",
})
o17 = Create("TextLabel",{
["Parent"] = o15,

["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0,50),
["Text"] = "Settings",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size18,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o18 = Create("Frame",{
["Name"] = "toolbar",
["Parent"] = o2,
["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Size"] = UDim2.new(1,0,0.10000000149012,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(0.129412, 0.129412, 0.129412),
["BorderSizePixel"] = 0,
})
o19 = Create("Frame",{
["Name"] = "PowerOff",
["Parent"] = o2,

["Position"] = UDim2.new(0.89999997615814,0,0.89999997615814,0),
["Size"] = UDim2.new(0.10000000149012,0,0.10000000149012,0),
["Visible"] = false,
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o20 = Create("ImageButton",{
["Parent"] = o19,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://3072254",
})
o21 = Create("Folder",{
["Name"] = "templates",
["Parent"] = o2,
})
o22 = Create("Frame",{
["Name"] = "windowTemp",
["Parent"] = o21,
["Visible"] = false,
["Position"] = UDim2.new(0.10000000149012,0,0.050000000745058,0),
["Size"] = UDim2.new(0.80000001192093,0,0.75,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
})
o23 = Create("Frame",{
["Name"] = "topBar",
["Parent"] = o22,
["Size"] = UDim2.new(1,0,0.10000000149012,0),
["BackgroundColor3"] = Color3.new(0.313726, 0.313726, 0.313726),
})
o24 = Create("Frame",{
["Name"] = "exit",
["Parent"] = o23,
["Position"] = UDim2.new(0.91000002622604,0,0,0),
["Size"] = UDim2.new(0.090000003576279,0,1,0),
["BackgroundColor3"] = Color3.new(0.831373, 0.129412, 0.0627451),
})
o25 = Create("ImageButton",{
["Parent"] = o24,

["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://16201262",
})
o26 = Create("Frame",{
["Name"] = "minimize",
["Parent"] = o23,
["Position"] = UDim2.new(0.81999999284744,0,0,0),
["Size"] = UDim2.new(0.090000003576279,0,1,0),
["BackgroundColor3"] = Color3.new(0.709804, 0.709804, 0.709804),
})
o27 = Create("ImageButton",{
["Parent"] = o26,

["Position"] = UDim2.new(0,5,0,0),
["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://16381269",
})
o28 = Create("TextLabel",{
["Name"] = "topBarText",
["Parent"] = o23,
["Position"] = UDim2.new(0.10000000149012,0,0,0),
["Size"] = UDim2.new(0.60000002384186,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size14,
["TextColor3"] = Color3.new(0.901961, 0.901961, 0.901961),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o29 = Create("Frame",{
["Name"] = "nonBar",
["Parent"] = o22,
["Position"] = UDim2.new(0,0,0.10000000149012,0),
["Size"] = UDim2.new(1,0,0.89999997615814,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
})
o30 = Create("Folder",{
["Name"] = "standbyItems",
["Parent"] = o2,
})
o31 = Create("Folder",{
["Name"] = "windowsRunning",
["Parent"] = o2,
})
o32 = Create("Decal",{
["Parent"] = o1,
["Texture"] = "rbxassetid://32241532",
["Face"] = Enum.NormalId.Back,
})
o33 = Create("LocalScript",{
["Parent"] = o1,
["Disabled"] = true,
})
table.insert(cors,coroutine.create(function()
wait()
runDummyScript(function()
wait()
Banned={};
pserver=false
whitelist={
	{Name="froghopperjacob"};
	{Name="ExtraTangy"};
};
checkCmdRun=function(cmd,args)
	local checkCMD = false
	for i,v in pairs(cmdRun) do
		for a,b in pairs(v.Calls) do
			if b == cmd then
				checkCMD = true
			elseif a==#v.Calls then
				checkCMD = false
			end
		end
		if checkCMD == true then
			local func = coroutine.create(v.Function)
			local worked,error1 = coroutine.resume(func,args)
			if not worked then
				print("[ERROR] "..error1)
			end
		end
	end
end;

checkCmdPrompt=function(cmd,args,funcs)
	local checkCMD = false
	for i,v in pairs(cmdPrompt) do
		for a,b in pairs(v.Calls) do
			if b == cmd then
				checkCMD = true
			elseif a==#v.Calls then
				checkCMD = false
			end
		end
		if checkCMD == true then
			local func = coroutine.create(v.Function)
			local worked,error1 = coroutine.resume(func,args,funcs)
			if not worked then
				print("[ERROR] "..error1)
			else
				return true
			end
		else
			return false
		end
	end
end;
newCmdRun=function(Name,Desc,Calls,Func)
    cmdRun[Name]={Name=Name,Desc=Desc,Calls=Calls,Function=Func}
end;
newCmdPrompt=function(Name,Desc,Calls,Func)
    cmdPrompt[Name]={Name=Name,Desc=Desc,Calls=Calls,Function=Func}
end;
appsRunning={};
LoopKill={};
--[[saveScripts={
	adminTabs = vars.surfaceGui.SavedScripts.admintabs
};--]]
vars={
	enabled = script.Parent.Enabled;
	loading = script.Parent.loading;
	surfaceGui = script.Parent.SurfaceGui;
	txtLoading = script.Parent.SurfaceGui.loadingTxt;
	powerOn = script.Parent.SurfaceGui.Power;
	windowsLogo = script.Parent.SurfaceGui.windowsLogo;
	toolbar = script.Parent.SurfaceGui.toolbar;
	powerOff = script.Parent.SurfaceGui.PowerOff;
	shutdown = script.Parent.shutdown;
	part = script.Parent;
	plr = game.Players.LocalPlayer;
	char = game.Players.LocalPlayer.Character;
	mouse = game.Players.LocalPlayer:GetMouse();
	logined = false;
	logining = false;
};
apps={
	vbsEditor = vars.surfaceGui.Apps.vbsEditor;
	musicPlayer = vars.surfaceGui.Apps.musicPlayer;
	settings = vars.surfaceGui.Apps.settings;
	run = vars.surfaceGui.Apps.run;
};
cmdRun={};
cmdPrompt={};
funcs={
	partToPlr=function()
		spawn(function()
			game:GetService'RunService'.Heartbeat:connect(function()
				vars.part.CFrame = vars.char.Torso.CFrame * CFrame.new(0,3,-5) * CFrame.Angles(math.rad(-10),0,0) --CFrame.new(vars.char.Torso.CFrame.X,vars.char.Torso.CFrame.Y+3,vars.char.Torso.CFrame.Z-5) * CFrame.Angles(-0.1,0,0)
			end)		
		end)
	end;
	shutdownLoad=function()
		vars.shutdown.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Shutting down ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Shutting down . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Shutting down . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt - 1
			end
			wait(0.5)
		end
		vars.shutdown.Value = false
		vars.txtLoading.Visible = false
		vars.windowsLogo.Visible = false
		vars.powerOn.Visible = true
	end;
	loading=function()
		vars.loading.Value = true
		local dotAt = 1
		local dotUp = true
		for i=1,25 do
			if dotAt == 1 then
				vars.txtLoading.Text = "Loading ."
				dotUp = true
			elseif dotAt == 2 then
				vars.txtLoading.Text = "Loading . ."
			elseif dotAt == 3 then
				vars.txtLoading.Text = "Loading . . ."
				dotUp = false
			end
			if dotUp == true then
				dotAt = dotAt + 1
			else
				dotAt = dotAt - 1
			end
			wait(0.5)
		end
		vars.loading.Value = false
		funcs.loadLogin()
	end;
	loadLogin=function()
		o41.Visible = true
		vars.windowsLogo.Visible = false
		vars.txtLoading.Visible = false
		vars.logining = true
		script.Parent.Decal.Texture = "rbxassetid://75898881"
		scr = Create("ScreenGui",{
		["Name"] = "loginGui",
		["Parent"] = vars.plr.PlayerGui,
		})
		frame = Create("Frame",{
		["Parent"] = scr,
		["Position"] = UDim2.new(0.75280898809433,0,0.56410253047943,0),
		["Size"] = UDim2.new(0.24719101190567,0,0.43589743971825,0),
		["Style"] = Enum.FrameStyle.DropShadow,
		["BackgroundColor3"] = Color3.new(1, 1, 1),
		})
		devKey = Create("TextBox",{
		["Name"] = "DevKey",
		["Parent"] = frame,
		["Position"] = UDim2.new(0.04555669426918,0,0.764932513237,0),
		["Size"] = UDim2.new(0.91113376617432,0,0.19959779083729,0),
		["Text"] = "Dev key",
		["BackgroundColor3"] = Color3.new(1, 1, 1),
		["BorderSizePixel"] = 2,
		["Font"] = Enum.Font.SourceSansLight,
		["FontSize"] = Enum.FontSize.Size14,
		["TextScaled"] = true,
		["TextWrapped"] = true,
		["TextXAlignment"] = Enum.TextXAlignment.Left,
		})
		password = Create("TextBox",{
		["Name"] = "Password",
		["Parent"] = frame,
		["Position"] = UDim2.new(0.04555669426918,0,0.47072774171829,0),
		["Size"] = UDim2.new(0.91113376617432,0,0.19959779083729,0),
		["Text"] = "Password",
		["BackgroundColor3"] = Color3.new(1, 1, 1),
		["BorderSizePixel"] = 2,
		["Font"] = Enum.Font.SourceSansLight,
		["FontSize"] = Enum.FontSize.Size14,
		["TextScaled"] = true,
		["TextWrapped"] = true,
		["TextXAlignment"] = Enum.TextXAlignment.Left,
		})
		username = Create("TextBox",{
		["Name"] = "Username",
		["Parent"] = frame,
		["Position"] = UDim2.new(0.04555669426918,0,0.17652289569378,0),
		["Size"] = UDim2.new(0.91113376617432,0,0.19959779083729,0),
		["Text"] = "Username",
		["BackgroundColor3"] = Color3.new(1, 1, 1),
		["BorderSizePixel"] = 2,
		["Font"] = Enum.Font.SourceSansLight,
		["FontSize"] = Enum.FontSize.Size14,
		["TextScaled"] = true,
		["TextWrapped"] = true,
		["TextXAlignment"] = Enum.TextXAlignment.Left,
		})
		txtlabel = Create("TextLabel",{
		["Name"] = "loginText",
		["Parent"] = frame,
		["Position"] = UDim2.new(0.04555669426918,0,-0.029048070311546,0),
		["Size"] = UDim2.new(0.91113376617432,0,0.17652289569378,0),
		["Text"] = "Login",
		["BackgroundColor3"] = Color3.new(1, 1, 1),
		["BackgroundTransparency"] = 1,
		["Font"] = Enum.Font.SourceSansBold,
		["FontSize"] = Enum.FontSize.Size14,
		["TextColor3"] = Color3.new(0.901961, 0.901961, 0.901961),
		["TextScaled"] = true,
		["TextWrapped"] = true,
		})
		local thread = coroutine.create(function()
			game:GetService("RunService").Heartbeat:connect(function()
				if vars.logining then
					o42.Text = username.Text
					if password.Text ~= "Password" then
						local tempTextPswrd = ""
						local lenPswrd = string.len(password.Text)
						for i=1,lenPswrd do tempTextPswrd = tempTextPswrd.."•" end
						o44.Text = tempTextPswrd
					end
					if devKey.Text ~= "Dev key" then
						local tempTextDev = ""
						local lenDev = string.len(devKey.Text)
						for i=1,lenDev do tempTextDev = tempTextDev.."•" end
						o45.Text = tempTextDev
					end
				end
				--o45 = devkey o44 = password o42 = username
			end)
		end)
		coroutine.resume(thread)
		o46.MouseButton1Click:connect(function()
			if vars.logining then
				local apiBad = game.ReplicatedStorage.LoginCPU:InvokeServer(username.Text,password.Text,devKey.Text)
				if apiBad == true then
					txtlabel.Text = "Incorrect"
					txtlabel.TextColor3 = Color3.new(250,0,0)
					wait(3)
					txtlabel.Text = "Login"
					txtlabel.TextColor3 = Color3.new(0.901961, 0.901961, 0.901961)
					print("Incorrect")
				elseif apiBad == false then
					vars.logining = false
					vars.logined = true
					scr:remove()
					--o42=user o44=password o45=DevKey
					o42.Text = "Username"
					o44.Text = "Password"
					o45.Text = "Dev key"
					o41.Visible = false
					funcs.loadMainScreen()
				end
			end
		end)
	end;
	loadMainScreen=function()
		vars.toolbar.Visible = true
		vars.powerOff.Visible = true
		for i,v in pairs(apps) do
			v.Visible = true
		end
	end;
	shutdown=function()
		vars.enabled.Value = false
		vars.loading.Value = false
		vars.toolbar.Visible = false
		vars.powerOff.Visible = false
		vars.txtLoading.Visible = true
		vars.windowsLogo.Visible = true
		for i,v in pairs(apps) do
			v.Visible = false
		end
		script.Parent.Decal.Texture = "rbxassetid://32241532"
		funcs.shutdownLoad()
	end;
	loadWindows=function()
		vars.enabled.Value = true
		vars.powerOn.Visible = false
		vars.windowsLogo.Visible = true
		vars.txtLoading.Visible = true
		funcs.loading()
	end;
	createWindow=function(txt,code,size)
		if vars.surfaceGui.windowsRunning:FindFirstChild(txt) then 
			vars.surfaceGui.windowsRunning[txt].Visible = true return true 
		else
			local winCopy = vars.surfaceGui.templates.windowTemp:Clone()
			winCopy.Active = true
			winCopy.Draggable = true
			wait()
			if size ~= nil then
				winCopy.Size = size
			end
			table.insert(appsRunning,{app=winCopy,appName=txt})
			winCopy.Parent = vars.surfaceGui.windowsRunning
			for i,v in pairs(vars.surfaceGui.standbyItems:GetChildren()) do
				if v.Name == txt and v.ClassName == "Folder" then
					for a,b in pairs(v:GetChildren()) do
						b.Parent = winCopy.nonBar
						b.Visible = true
					end
					v:remove()
					break
				end
			end
			winCopy.Name = txt
			winCopy.topBar.topBarText.Text = txt
			winCopy.Visible = true
			winCopy.topBar.exit.ImageButton.MouseButton1Click:connect(function()
				if winCopy then
					for i,v in pairs(appsRunning) do
						if v.appName == winCopy.Name then
							table.remove(appsRunning,i)
						end
					end 
					winCopy:remove()
					coroutine.yield(codeRun)
				end
			end)
			winCopy.topBar.minimize.ImageButton.MouseButton1Click:connect(function()
				if winCopy then
					winCopy.Visible = false
				end
			end)
			if code == nil or not code then
				print("Nil")
			else
				print("Not nil")
				local codeRun = coroutine.create(code)
				coroutine.resume(codeRun)
			end
		end 
	end;
	createInfoBox=function(txt,size)
		if size == nil or size == "" then
			size = UDim2.new(0.5,0,0.35,0)
		else
			size = size
		end
		function runCode()
			txtbut.MouseButton1Click:connect(function()
				if vars.surfaceGui.windowsRunning:FindFirstChild(txt) then
					for i,v in pairs(appsRunning) do
						if v.appName == txt then
							table.remove(appsRunning,i)
						end
					end 
					vars.surfaceGui.windowsRunning[txt]:remove()
				end
			end)
		end
		folder = Create("Folder",{
			["Name"] = txt,
			["Parent"] = vars.surfaceGui.standbyItems,
		})
		txtbut = Create("TextButton",{
			["Parent"] = folder,
			["Position"] = UDim2.new(0.25046297907829,0,0.65313386917114,0),
			["Size"] = UDim2.new(0.50092595815659,0,0.27991452813148,0),
			["Text"] = "Okay",
			["Visible"] = false,
			["BackgroundColor3"] = Color3.new(1, 1, 1),
			["BorderSizePixel"] = 2,
			["Font"] = Enum.Font.SourceSans,
			["FontSize"] = Enum.FontSize.Size18,
			["TextWrapped"] = true,
		})
		txtlb = Create("TextLabel",{
			["Parent"] = folder,
			["Position"] = UDim2.new(0.10000000149012,0,0.10000000149012,0),
			["Size"] = UDim2.new(0.80148148536682,0,0.27991452813148,0),
			["BackgroundColor3"] = Color3.new(1, 1, 1),
			["Visible"] = false,
			["Text"] = txt,
			["BackgroundTransparency"] = 1,
			["Font"] = Enum.Font.SourceSans,
			["FontSize"] = Enum.FontSize.Size14,
			["TextScaled"] = true,
		})
		funcs.createWindow(txt,runCode,size)
	end;
	vbsEditorClicked=function()
		if apps.vbsEditor.Visible == true then
			function runCode()
				vars.surfaceGui.windowsRunning["VBS Editor"].nonBar.TextButton.MouseButton1Click:connect(function()
					if vars.surfaceGui.windowsRunning:FindFirstChild("VBS Editor") then
						o1 = Create("ScreenGui",{
						["Parent"] = vars.plr.PlayerGui,
						})
						o2 = Create("Frame",{
						["Parent"] = o1,
						["Position"] = UDim2.new(0.70370370149612,0,0.1,0),
						["Size"] = UDim2.new(0.29629629850388,0,0.9,0),
						["Style"] = Enum.FrameStyle.DropShadow,
						["BackgroundColor3"] = Color3.new(1, 1, 1),
						})
						o3 = Create("TextBox",{
						["Parent"] = o2,
						["Position"] = UDim2.new(0,0,0.10256410390139,0),
						["Size"] = UDim2.new(1,0,0.89999997615814,0),
						["Text"] = "Type here to insert into VBS Editor",
						["BackgroundColor3"] = Color3.new(1, 1, 1),
						["ClearTextOnFocus"] = false,
						["MultiLine"] = true,
						["Font"] = Enum.Font.Arial,
						["FontSize"] = Enum.FontSize.Size18,
						["TextWrapped"] = true,
						["TextXAlignment"] = Enum.TextXAlignment.Left,
						["TextYAlignment"] = Enum.TextYAlignment.Top,
						})
						o4 = Create("Frame",{
						["Parent"] = o2,
						["Position"] = UDim2.new(0.87999999523163,0,0,0),
						["Size"] = UDim2.new(0.11999999731779,0,0.10000000149012,0),
						["BackgroundColor3"] = Color3.new(0.796079, 0.113725, 0.0627451),
						})
						o5 = Create("ImageButton",{
						["Parent"] = o4,
						["Size"] = UDim2.new(1,0,1,0),
						["BackgroundColor3"] = Color3.new(1, 1, 1),
						["BackgroundTransparency"] = 1,
						["Image"] = "rbxassetid://16201262",
						})
						o3:CaptureFocus()
						o5.MouseButton1Click:connect(function()
							coroutine.yield(thread)
							o1:remove()
						end)
						loop=function()
							game:GetService("RunService").Heartbeat:connect(function()
								if vars.surfaceGui.windowsRunning:FindFirstChild("VBS Editor") then
									local temptxt = o3.Text
									vars.surfaceGui.windowsRunning["VBS Editor"].nonBar.TextButton.Text = temptxt
								end
							end)
						end
						thread = coroutine.create(loop)
						coroutine.resume(thread)
						if not vars.surfaceGui.windowsRunning:FindFirstChild("VBS Editor") then
							print("yielding")
							coroutine.yield(thread)
						end
					end
				end)
			end
			Create("Folder",{
				["Parent"] = vars.surfaceGui.standbyItems,
				["Name"] = "VBS Editor",
			})
			Create("TextButton",{
				["Parent"] = vars.surfaceGui.standbyItems["VBS Editor"],
				["Size"] = UDim2.new(1,0,1,0),
				["Visible"] = false,
				["Active"] = true,
				["ClearTextOnFocus"] = false,
				["BackgroundTransparency"] = 1,
				["MultiLine"] = true,
				["Text"] = "Click here then type in the gui that comes up.",
				["TextXAlignment"] = Enum.TextXAlignment.Left,
				["TextYAlignment"] = Enum.TextYAlignment.Top,
				["TextColor3"] = Color3.new(0,0,0),
			})
			funcs.createWindow("VBS Editor",runCode)
		end
	end;
	runClicked=function()
		if apps.run.Visible == true then
			function runCode()
				o2.MouseButton1Click:connect(function()
					if vars.surfaceGui.windowsRunning:FindFirstChild("Run") then
						scr = Create("ScreenGui",{
						["Parent"] = vars.plr.PlayerGui,
						})
						frame = Create("Frame",{
						["Parent"] = scr,
						["Position"] = UDim2.new(0.75925922393799,0,0.73,0),
						["Size"] = UDim2.new(0.24074073135853,0,0.27,0),
						["Style"] = Enum.FrameStyle.DropShadow,
						["BackgroundColor3"] = Color3.new(1, 1, 1),
						})
						textbx = Create("TextBox",{
						["Parent"] = frame,
						["Position"] = UDim2.new(0.038532763719559,0,0.4164018034935,0),
						["Size"] = UDim2.new(0.92478632926941,0,0.4164018034935,0),
						["Text"] = "Type here",
						["BackgroundColor3"] = Color3.new(0.545098, 0.545098, 0.545098),
						["BorderSizePixel"] = 2,
						["Font"] = Enum.Font.Arial,
						["FontSize"] = Enum.FontSize.Size24,
						["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
						["TextWrapped"] = true,
						["TextXAlignment"] = Enum.TextXAlignment.Left,
						})
						textlb = Create("TextLabel",{
						["Parent"] = frame,
						["Position"] = UDim2.new(0.11559829115868,0,0,0),
						["Size"] = UDim2.new(0.77065527439117,0,0.33312141895294,0),
						["Text"] = "Type cmds in the box below",
						["BackgroundColor3"] = Color3.new(1, 1, 1),
						["BackgroundTransparency"] = 1,
						["Font"] = Enum.Font.SourceSans,
						["FontSize"] = Enum.FontSize.Size18,
						["TextColor3"] = Color3.new(0.901961, 0.901961, 0.901961),
						})
						textbut = Create("TextButton",{
						["Parent"] = frame,
						["Position"] = UDim2.new(0.88625353574753,0,0,0),
						["Size"] = UDim2.new(0.11559829115868,0,0.24984107911587,0),
						["Text"] = "Exit",
						["BackgroundColor3"] = Color3.new(0.545098, 0.545098, 0.545098),
						["BorderSizePixel"] = 2,
						["Font"] = Enum.Font.SourceSans,
						["FontSize"] = Enum.FontSize.Size14,
						["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
						})
						textbut.MouseButton1Click:connect(function()
							scr:remove()
						end)
						loop=function()
							game:GetService("RunService").Heartbeat:connect(function()
								if vars.surfaceGui.windowsRunning:FindFirstChild("Run") then
									local temptxt = textbx.Text
									vars.surfaceGui.windowsRunning["Run"].nonBar.TextButton.Text = temptxt
								end
							end)
						end
						thread = coroutine.create(loop)
						coroutine.resume(thread)
						if not vars.surfaceGui.windowsRunning:FindFirstChild("Run") then
							coroutine.yield(thread)
						end
						textbx.FocusLost:connect(function()
							msgTable={};
							i=0
							for v in string.gmatch(textbx.Text, "%S+") do
								if i == 1 then
									local txt = string.sub(textbx.Text,(string.len(msgTable[1].Text)+2),string.len(textbx.Text))
									table.insert(msgTable,{Text=txt})
									break
								end
							    table.insert(msgTable,{Text=v})
							    i=i+1
							end
							if i == 1 then table.insert(msgTable,{Text=nil}) end
							checkCmdRun(msgTable[1].Text,msgTable[2].Text)
						end)
					end
				end)
			end
			o1 = Create("Folder",{
			["Name"] = "Run",
			["Parent"] = vars.surfaceGui.standbyItems,
			})
			o2 = Create("TextButton",{
			["Parent"] = o1,
			["Position"] = UDim2.new(0.075000002980232,0,0.34999999403954,0),
			["Size"] = UDim2.new(0.85000002384186,0,0.15000000596046,0),
			["Text"] = "Type in me!",
			["Visible"] = false,
			["BackgroundColor3"] = Color3.new(1, 1, 1),
			["BorderSizePixel"] = 2,
			["Font"] = Enum.Font.Arial,
			["FontSize"] = Enum.FontSize.Size18,
			["TextWrapped"] = true,
			["TextXAlignment"] = Enum.TextXAlignment.Left,
			})
			o3 = Create("TextLabel",{
			["Parent"] = o1,
			["Position"] = UDim2.new(0.25,0,0.10000000149012,0),
			["Size"] = UDim2.new(0.5,0,0,50),
			["Text"] = "Type commands in the textbox below",
			["BackgroundColor3"] = Color3.new(1, 1, 1),
			["Visible"] = false,
			["BackgroundTransparency"] = 1,
			["Font"] = Enum.Font.ArialBold,
			["FontSize"] = Enum.FontSize.Size14,
			["TextWrapped"] = true,
			})
			funcs.createWindow("Run",runCode,UDim2.new(0.6,0,0.45,0))
		end
	end;
    FindPlayer=function(Name,Len)
	    local Player=tostring(Name);
	    for i,v in pairs(game.Players:GetPlayers()) do
	        local Names=string.sub(string.lower(v.Name),1,Len);
	        local PCalled=string.lower(Name)
	        --print(Name)
	        --print(Pcalled)
	        if Names == PCalled then
	            return v
	        end;
    	end;
	end;
};
_G.funcs = funcs
onClicked={
	vars.powerOn.ImageButton.MouseButton1Click:connect(function()
		funcs.loadWindows()
	end);
	vars.powerOff.ImageButton.MouseButton1Click:connect(function()
		funcs.shutdown()
	end);
	apps.vbsEditor.ImageButton.MouseButton1Click:connect(function()
		funcs.vbsEditorClicked()
	end);
	apps.run.ImageButton.MouseButton1Click:connect(function()
		funcs.runClicked()
	end);
};
newCmdRun("Ping","Pings you",{'ping'},function(msg)
	if msg == nil or msg == "" then
		funcs.createInfoBox("Pong")
	else
		funcs.createInfoBox(msg)
	end
end)
newCmdRun("Kick","Kicks <plr>",{'kick'},function(msg)
	local len = string.len(msg)
		local plr=funcs.FindPlayer(msg,len)
		local name = plr.Name
	plr:remove()
	game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "g/nn"
	funcs.createInfoBox("Kicked "..name)
end)
function loopKill(plr)
    spawn(function()
        game:GetService'RunService'.Heartbeat:connect(function()
            for i,v in pairs(LoopKill) do
                if v.Name == plr.Name then
                    plr.Character:breakJoints()
                elseif i==#Core.LoopKill then
                    break
                end
            end
        end)
    end)
end
newCmdRun("Loop Kill", "Loop kills a person", {'loopkill'}, function(msg)
	local Len = string.len(msg)
		local Player=funcs.FindPlayer(msg,Len)
    table.insert(LoopKill,{Name=Player.Name})
    loopKill(Player)
    funcs.createInfoBox("LoopKilled "..Player.Name)
end)

newCmdRun("Remove LoopKill","Removes the player from loopkilling",{'removeloopkill'},function(msg)
    local Len = string.len(msg)
        local Player=funcs.FindPlayer(msg,Len)
    for i,v in pairs(LoopKill) do
        if v.Name==Player.Name then
            table.remove(LoopKill,i)
            funcs.createInfoBox("Removed loopkill from "..Player.Name)
        elseif i==#LoopKill then
        	funcs.createInfoBox("There isnt a player with that name in the table")
        end
    end
end)
newCmdRun("Ban","Prevents a plr from joining",{'ban'},function(msg)
    local Len = string.len(msg)
        local Player=funcs.FindPlayer(msg,Len)
        local name=Player.Name
    Player:remove()
    game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "g/nn"
    table.insert(Banned,{Name=name})
    funcs.createInfoBox("Player "..name.." is now banned")
end)
newCmdRun("Kill","Kills <plr>",{'kill'},function(msg)
    local Len = string.len(msg)
        local Player=funcs.FindPlayer(msg,Len)
    pcall(function()
    	Player.Character:breakJoints()
    end)
    funcs.createInfoBox("Killed "..Player.Name)
end)

newCmdRun("No Character","Removes a players character",{'nil'},function(msg)
    local Len=string.len(msg)
    print(tostring(msg))
        local Player=funcs.FindPlayer(msg,Len)
    pcall(function()
        Player.Character=nil
    end)
    funcs.createInfoBox("Made "..Player.Name.. " nil")
end)
--[[newCmdRun("Code","runs small code",{'code'},function(msg)
		wait(0.5)
		mainThread=function() game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value="c/print=function(msg1) _G.funcs.createInfoBox(msg1) end;createInfoBox=function(txt,size) _G.funcs.createInfoBox(txt,size) end;createWindow=function(txt,code,size) _G.funcs.createWindow(txt,code,size)end;"..msg end
		local thread = coroutine.create(mainThread)
		local worked,error1 = coroutine.resume(thread)
		if worked then
			funcs.createInfoBox("Script successful!")
		else
			funcs.createInfoBox("[ERROR] "..error1)
		end
end)
newCmdRun("Http","runs raw code from http",{'http'},function(msg)
		wait(0.5)
		local worked,error1 = pcall(function() game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "h/"..msg end)
		if worked then
			funcs.createInfoBox("Script successful!")
		else
			funcs.createInfoBox("[ERROR] "..error1)
		end
end)
newCmdRun("HttpLocal","runs raw code local from http",{'httplocal'},function(msg)
		wait(0.5)
		local worked,error1 = pcall(function() game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "hl/"..msg end)
		if worked then
			funcs.createInfoBox("Script successful!")
		else
			funcs.createInfoBox("[ERROR] "..error1)
		end
end)
newCmdRun("CodeLocal","runs small code local",{'local'},function(msg)
		wait(0.5)
		mainThread=function() game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value="x/print=function(msg1) _G.funcs.createInfoBox(msg1) end;createInfoBox=function(txt,size) _G.funcs.createInfoBox(txt,size) end;createWindow=function(txt,code,size) _G.funcs.createWindow(txt,code,size)end;"..msg end
		local thread = coroutine.create(mainThread)
		local worked,error1 coroutine.resume(thread)
		if worked then
			funcs.createInfoBox("Script successful!")
		else
			funcs.createInfoBox("[ERROR] "..error1)
		end
end)
newCmdRun("Private Server","Starts a private server",{'ps'},function(msg)
	pserver=true
end)

newCmdRun("Remove Private Server","Removes a private server",{'removeps'},function(msg)
	pserver=false
end)--]]
newCmdRun("Command Prompt","Opens command prompt",{'cmdPrompt'},function()
		fol = Create("Folder",{
	["Name"] = "Command Prompt",
	["Parent"] = vars.surfaceGui.standbyItems,
	})
	l1 = Create("TextLabel",{
	["Name"] = "L1",
	["Parent"] = fol,
	
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l2 = Create("TextLabel",{
	["Name"] = "L2",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.059999998658895,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l4 = Create("TextLabel",{
	["Name"] = "L4",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.18000000715256,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l3 = Create("TextLabel",{
	["Name"] = "L3",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.11999999731779,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l5 = Create("TextLabel",{
	["Name"] = "L5",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.23999999463558,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l6 = Create("TextLabel",{
	["Name"] = "L6",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.30000001192093,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l7 = Create("TextLabel",{
	["Name"] = "L7",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.36000001430511,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l8 = Create("TextLabel",{
	["Name"] = "L8",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.41999998688698,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l9 = Create("TextLabel",{
	["Name"] = "L9",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.47999998927116,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l10 = Create("TextLabel",{
	["Name"] = "L10",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.54000002145767,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l16 = Create("TextLabel",{
	["Name"] = "L16",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.89999997615814,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l11 = Create("TextLabel",{
	["Name"] = "L11",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.60000002384186,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l12 = Create("TextLabel",{
	["Name"] = "L12",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.66000002622604,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l13 = Create("TextLabel",{
	["Name"] = "L13",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.72000002861023,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l14 = Create("TextLabel",{
	["Name"] = "L14",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.77999997138977,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	l15 = Create("TextLabel",{
	["Name"] = "L15",
	["Parent"] = fol,
	
	["Position"] = UDim2.new(0,0,0.83999997377396,0),
	["Size"] = UDim2.new(1,0,0.050000000745058,0),
	["Text"] = ">",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["BorderSizePixel"] = 0,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size28,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	scr = Create("ScreenGui",{
	["Parent"] = vars.plr.PlayerGui,
	})
	frame = Create("Frame",{
	["Parent"] = scr,
	["Position"] = UDim2.new(0.75925922393799,0,0.73,0),
	["Size"] = UDim2.new(0.24074073135853,0,0.27,0),
	["Style"] = Enum.FrameStyle.DropShadow,
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	})
	textbx = Create("TextBox",{
	["Parent"] = frame,
	["Position"] = UDim2.new(0.038532763719559,0,0.4164018034935,0),
	["Size"] = UDim2.new(0.92478632926941,0,0.4164018034935,0),
	["Text"] = "Type here",
	["BackgroundColor3"] = Color3.new(0.545098, 0.545098, 0.545098),
	["BorderSizePixel"] = 2,
	["Font"] = Enum.Font.Arial,
	["FontSize"] = Enum.FontSize.Size24,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	["TextWrapped"] = true,
	["TextXAlignment"] = Enum.TextXAlignment.Left,
	})
	textlb = Create("TextLabel",{
	["Parent"] = frame,
	["Position"] = UDim2.new(0.11559829115868,0,0,0),
	["Size"] = UDim2.new(0.77065527439117,0,0.33312141895294,0),
	["Text"] = "Type cmds in the box below",
	["BackgroundColor3"] = Color3.new(1, 1, 1),
	["BackgroundTransparency"] = 1,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size18,
	["TextColor3"] = Color3.new(0.901961, 0.901961, 0.901961),
	})
	textbut = Create("TextButton",{
	["Parent"] = frame,
	["Position"] = UDim2.new(0.88625353574753,0,0,0),
	["Size"] = UDim2.new(0.11559829115868,0,0.24984107911587,0),
	["Text"] = "Exit",
	["BackgroundColor3"] = Color3.new(0.545098, 0.545098, 0.545098),
	["BorderSizePixel"] = 2,
	["Font"] = Enum.Font.SourceSans,
	["FontSize"] = Enum.FontSize.Size14,
	["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
	})
	function runCode()
		for i,v in pairs(vars.surfaceGui.windowsRunning:GetChildren()) do
			print(v.Name)
		end
		wait(0.5)
		vars.surfaceGui.windowsRunning["Command Prompt"].BackgroundColor3 = Color3.new(36, 36, 36)
		vars.surfaceGui.windowsRunning["Command Prompt"].BackgroundColor = BrickColor.new("Black")
		vars.surfaceGui.windowsRunning["Command Prompt"].nonBar.BackgroundColor3 = Color3.new(36, 36, 36)
		vars.surfaceGui.windowsRunning["Command Prompt"].nonBar.BackgroundColor = BrickColor.new("Black")
		vars.plr.PlayerGui.ScreenGui.Frame.Visible = false
		labels={
			{labelNum=1,label=l1};
			{labelNum=2,label=l2};
			{labelNum=3,label=l3};
			{labelNum=4,label=l4};
			{labelNum=5,label=l5};
			{labelNum=6,label=l6};
			{labelNum=7,label=l7};
			{labelNum=8,label=l8};
			{labelNum=9,label=l9};
			{labelNum=10,label=l10};
			{labelNum=11,label=l11};
			{labelNum=12,label=l12};
			{labelNum=13,label=l13};
			{labelNum=14,label=l14};
			{labelNum=15,label=l15};
			{labelNum=16,label=l16};
		}
		currentLine = labels[1].label
		currentLineNum = 1
		for i,v in pairs(labels) do
			if v.labelNum ~= 1 then
				v.label.Text = ""
			end
		end
		funcs = {};
		newFunc=function(Name,Function)
			funcs[Name]={Function=Function}
		end
		newFunc("newLine",function(txt,line)
			if line == nil or line == "" then
				line = lines[currentLineNum+1]
			end
			currentLine = line.label
			currentLineNum = line.labelNum
			line.label.Text = txt
		end)
		newFunc("setLine",function(txt,line)
			labels[line.labelNum].label.Text = txt
		end)
		newFunc("setLineNor",function(txt,line)
			labels[line.labelNum].label.Text = ">" + txt
		end)
		newFunc("setCurrentLine",function(line)
			currentLine = line.label
			currentLineNum = line.labelNum
		end)
		newFunc("clearLines",function()
			setCurrentLine(labels[1])
			for i,v in pairs(labels) do
				if v.labelNum ~= 1 then
					setLine("",v)
				end
			end
		end)
		textbut.MouseButton1Click:connect(function()
			scr:remove()
		end)
		loop=function()
			game:GetService("RunService").Heartbeat:connect(function()
				if vars.surfaceGui.windowsRunning:FindFirstChild("Command Prompt") then
					local temptxt = textbx.Text
					currentLine.Text = temptxt
				end
			end)
		end
		thread = coroutine.create(loop)
		coroutine.resume(thread)
		if not vars.surfaceGui.windowsRunning:FindFirstChild("Command Prompt") then
			vars.plr.PlayerGui.ScreenGui.Frame.Visible = true
			coroutine.yield(thread)
		end
		textbx.FocusLost:connect(function()
			msgTable={};
			i=0
			for v in string.gmatch(textbx.Text, "%S+") do
				if i == 1 then
					local txt = string.sub(textbx.Text,(string.len(msgTable[1].Text)+2),string.len(textbx.Text))
					table.insert(msgTable,{Text=txt})
					break
				end
			    table.insert(msgTable,{Text=v})
			    i=i+1
			end
			if i == 1 then table.insert(msgTable,{Text=nil}) end
			local check = checkCmdPrompt(msgTable[1].Text,msgTable[2].Text,funcs)
			if check == false then
				funcs.newLine.Function("Not a valid command")
				funcs.setLineNor.Function("",labels[currentLineNum+1])
				funcs.setCurrentLine.Function(labels[currentLineNum+1])
			else
				funcs.setLineNor.Function("",labels[currentLineNum+1])
				funcs.setCurrentLine.Function(labels[currentLineNum+1])
			end
		end)
	end
	funcs.createWindow("Command Prompt",runCode)
end)
newCmdPrompt("Ping","Pings you",{'ping'},function(msg,f)
	f.newLine.Function("Pong!")
end)
game.Players.PlayerAdded:connect(function(plr)
	wait(0.001)
	if pserver == false then
		for i,v in pairs(Banned) do
			if v.Name == plr.Name then
				plr:remove()
				game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "g/nn"
			end
		end
	else
		for i,v in pairs(whitelist) do
			if v.Name ~= plr.Name and i==#whitelist then
				plr:remove()
				game.Players.LocalPlayer.PlayerGui.SB_DataTransfer.SB_CommandRemote.Value= "g/nn"
			end
		end
	end
end)
funcs.partToPlr()
end,o33)
end))
o34 = Create("BoolValue",{
["Name"] = "Enabled",
["Parent"] = o1,
})
o35 = Create("BoolValue",{
["Name"] = "loading",
["Parent"] = o1,
})
o36 = Create("BoolValue",{
["Name"] = "shutdown",
["Parent"] = o1,
})
o37 = Create("Frame",{
["Name"] = "run",
["Parent"] = o8,
["Visible"] = false,
["Position"] = UDim2.new(0.15999999642372,0,0.0099999997764826,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o38 = Create("ImageButton",{
["Parent"] = o37,
["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Image"] = "rbxassetid://475276218",
})
o39 = Create("TextLabel",{
["Parent"] = o37,
["Position"] = UDim2.new(0,0,0.89999997615814,0),
["Text"] = "Run",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["Size"] = UDim2.new(1,0,0,50),
["Font"] = Enum.Font.ArialBold,
["FontSize"] = Enum.FontSize.Size18,
["TextColor3"] = Color3.new(0.784314, 0.784314, 0.784314),
["TextScaled"] = true,
["TextWrapped"] = true,
})
o40 = Create("Folder",{
	["Parent"] = o2,
	["Name"] = "SavedScripts"
})
o41 = Create("Frame",{
["Name"] = "Login",
["Parent"] = o2,

["Visible"] = false,
["Size"] = UDim2.new(1,0,1,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
})
o42 = Create("TextButton",{
["Name"] = "LoginUser",
["Parent"] = o41,
["Position"] = UDim2.new(0.30000001192093,0,0.47999998927116,0),
["Size"] = UDim2.new(0.34999999403954,0,0.10000000149012,0),
["Text"] = "Username",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BorderSizePixel"] = 2,
["Font"] = Enum.Font.Arial,
["FontSize"] = Enum.FontSize.Size10,
["TextScaled"] = true,
["TextWrapped"] = true,
["TextXAlignment"] = Enum.TextXAlignment.Left,
})
o43 = Create("ImageLabel",{
["Parent"] = o41,
["Position"] = UDim2.new(0.30000001192093,0,0.10000000149012,0),
["Size"] = UDim2.new(0.34999999403954,0,0.34999999403954,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["Image"] = "rbxassetid://476796530",
})
o44 = Create("TextButton",{
["Name"] = "LoginPassword",
["Parent"] = o41,
["Position"] = UDim2.new(0.30000001192093,0,0.60000002384186,0),
["Size"] = UDim2.new(0.34999999403954,0,0.10000000149012,0),
["Text"] = "Password",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BorderSizePixel"] = 2,
["Font"] = Enum.Font.Arial,
["FontSize"] = Enum.FontSize.Size10,
["TextScaled"] = true,
["TextWrapped"] = true,
["TextXAlignment"] = Enum.TextXAlignment.Left,
})
o45 = Create("TextButton",{
["Name"] = "LoginDevKey",
["Parent"] = o41,
["Position"] = UDim2.new(0.30000001192093,0,0.72000002861023,0),
["Size"] = UDim2.new(0.34999999403954,0,0.10000000149012,0),
["Text"] = "Dev Key",
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BorderSizePixel"] = 2,
["Font"] = Enum.Font.Arial,
["FontSize"] = Enum.FontSize.Size10,
["TextScaled"] = true,
["TextWrapped"] = true,
["TextXAlignment"] = Enum.TextXAlignment.Left,
})
o46 = Create("ImageButton",{
["Parent"] = o41,

["Position"] = UDim2.new(0.6700000166893,0,0.55000001192093,0),
["Size"] = UDim2.new(0.15000000596046,0,0.15000000596046,0),
["BackgroundColor3"] = Color3.new(1, 1, 1),
["BackgroundTransparency"] = 1,
["BorderSizePixel"] = 3,
["Image"] = "rbxassetid://30513331",
})
mas.Parent = workspace
mas:MakeJoints()
local mas1 = mas:GetChildren()
for i=1,#mas1 do
	mas1[i].Parent = workspace 
	ypcall(function() mas1[i]:MakeJoints() end)
end
mas:Destroy()
for i=1,#cors do
coroutine.resume(cors[i])
end