AllowedLoginName={
	{Name="froghopperjacob"};
	{Name="musdoy"};
	{Name="sonic7394"};
};
AllowedLoginPastebin={
	{Name="froghopperjacob"};
	{Name="Sonic1"};
	{Name="musdoy"};
};
local check = false
local login = null
for i,v in pairs(game.ReplicatedStorage:GetChildren()) do
	if v.Name == "LoginCPU" then
		check = true
		login = v
	end
end
if check == false then
	login = Instance.new("RemoteFunction",game.ReplicatedStorage)
	login.Name = "LoginCPU"
end
function login.OnServerInvoke(player,user,pass,devkey)
	h = game:GetService'HttpService'
	api_dev_key = devkey --Your Pastebin developer key goes here. Log in first, and then you can find it at pastebin.com/api
	api_user_key = '' --This is generated using the login info
	username = user --Your Pastebin username goes here
	password = pass --Your Pastebin password goes here
	apiBad=false
	allowedName=false
	allowedPastebin=false
	allowed=false
	BadApi={
		{Text="Bad API request, invalid api_dev_key"};
		{Text="Bad API request, invalid login"};
		{Text="Bad API request, account not active"};
	};
	api_user_key = h:PostAsync(
	    'http://pastebin.com/api/api_login.php',
	    'api_dev_key=' .. api_dev_key .. '&api_user_name=' .. username .. '&api_user_password=' .. password,
	    2
	)
	print(api_user_key) --DON'T DELETE THIS! IT IS ESSENTIAL FOR THE USER KEY TO BE GENERATED!
	for i,v in pairs(AllowedLoginName) do
		if v.Name == player.Name then
			allowedName = true
		end
	end
	for i,v in pairs(AllowedLoginPastebin) do
		if v.Name == username then
			allowedPastebin = true
			if allowedPastebin == true and allowedName == true then
				allowed = true
			end
		end
	end
	if allowed == true then
		for i,v in pairs(BadApi) do
			if string.lower(v.Text) == string.lower(api_user_key) then
				apiBad = true
			end
		end
	else
		apiBad=true
		print("Not allowed to log in")
	end
	print(apiBad)
	return apiBad
end